ifdef compact
	usermacros = -D COMPACT_PRINT
else
	usermacros =
endif

ifdef nanoflann
	nf_flag = -DTEST_NANOFLANN
else
	nf_flag =
endif

ifdef leaf_size
	ls_flag = -DLEAF_SIZE=$(leaf_size)
else
	ls_flag =
endif

benchmark_with_histogram: examples/benchmark_with_histogram.cpp
	g++ $(usermacros) $(ls_flag) -O3 -std=c++17 $< -lpthread -lstdc++fs -o $@

collatz_mt: examples/collatz_mt.cpp
	g++ $(usermacros) -O3 -std=c++17 $< -lpthread  -o $@

collatz_st: examples/collatz_st.cpp
	g++ $(usermacros) -O3 -std=c++17 $< -lpthread  -o $@

generate_1M_threaded: examples/generate_1M_threaded.cpp
	g++ -O3 -g -std=c++17 $< -lpthread  -o $@

generate_1M: examples/generate_1M.cpp
	g++ -O3 -std=c++17 $< -lpthread  -o $@

generate_knn: examples/generate_knn.cpp
	g++ -O3 -std=c++17 $< -lpthread  -o $@

generic_benchmark: examples/generic_benchmark.cpp
	g++ $(usermacros) $(ls_flag) $(nf_flag) -O3 -std=c++17 $< -lpthread -lstdc++fs -o $@

histogram_generator: examples/histogram_generator.cpp
	g++ $(usermacros) $(ls_flag) -O3 -std=c++17 $< -lpthread -lstdc++fs -o $@

iterator_locking: examples/iterator_locking.cpp
	g++ $(usermacros) $(ls_flag) -O3 -std=c++17 $< -lpthread -lstdc++fs -o $@

nanoflann_comparison: examples/nanoflann_comparison.cpp
	g++ -O3 examples/nanoflann_comparison.cpp -lpthread  -o nanoflann_comparison

templated_knn_benchmark: examples/templated_knn_benchmark.cpp
	g++ $(usermacros) $(ls_flag) -O3 -std=c++17 $< -lpthread -lstdc++fs -o $@

.PHONY: test
fnames := $(shell ls tests/suite/*.cpp | sed "s:tests/suite/::; s:.cpp::")
test:
	@$(foreach var, $(fnames), \
		g++ -O3 -std=c++17 tests/suite/$(var).cpp -lpthread -o $(var) || exit; \
		echo -n "$(var): "; \
		./$(var); \
		rm $(var);)

.PHONY: clean
clean:
	rm -f $(shell ls examples | sed "s/.cpp//") || true
