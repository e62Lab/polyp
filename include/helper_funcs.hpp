#pragma once

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <array>

/**
 * @brief Saves the data in MATLAB-friendly format.
 *
 * @tparam Point type of a data point
 * @param data point container
 * @param dest name of file where data should be saved
 */
template <int DIM, typename DT = double, typename Point = std::array<DT, DIM>>
void save_for_matlab(std::vector<Point> &data, std::string dest) {
    std::ofstream f_out(dest);
    f_out.precision(12);
    for (unsigned int i = 0; i < data.size(); i++) {
        for (unsigned int j = 0; j < data[i].size(); j++) {
            if (j > 0) f_out << " ";
            f_out << data[i][j];
        }
        f_out << std::endl;
    }
}

/**
 * @brief Calculates the distance between two closest points in the dataset.
 *
 * The algorithm is of quadratic complexity.
 * This function is meant for result verification and so correctness is more important than speed.
 *
 * @tparam DIM dimensionality of data
 * @tparam double scalar value type
 * @tparam std::array<DT, DIM> point value type
 * @param data container of points
 * @param eps optional expected minimum distance
 * @param offending optional counter of offending points
 * @return DT shortest distance between two points
 */
template <int DIM, typename DT = double, typename Point = std::array<DT, DIM>>
DT minimum_distance(std::vector<Point> &data, DT eps = 0., int *offending = nullptr) {
    // begin with largest possible value
    DT current = std::numeric_limits<DT>::infinity();
    if (offending != nullptr) {
        *offending = 0;
    }
    DT eps_sq = eps * eps;

    // consider all possible point pairs
    for (unsigned int i = 0; i < (int) data.size() - 1; i++) {
        for (unsigned int j = i + 1; j < (int) data.size(); j++) {
            // sum squares of all components
            DT temp, acc = 0.;
            for (unsigned int k = 0; k < DIM; k++) {
                temp = data[i][k] - data[j][k];
                acc += temp * temp;
            }
            // keep the sum if it's smaller than the current value
            if (acc < current) {
                current = acc;
            }
            // count offending
            if (acc < eps_sq) {
                *offending += 1;
            }
        }
    }

    // return the actual value, that is the square root of the current value
    return std::sqrt(current);
}
