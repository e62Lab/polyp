#pragma once

#include <sys/mman.h>  // *NIX specific, for Windows consider: mman-win32, memoryapi.h
#include <errno.h>

#include <string>
#include <stdexcept>
#include <iostream>


namespace vmc {

/**
 * @brief Thread safe lazily allocated container.
 * Uses Linux-specific system headers for memory management.
 *
 * @tparam ElementType type of elements to be stored
 */
template <typename ElementType>
class VirtualMemoryContainer {
private:
    size_t num_elements;
    ElementType* container;

public:
    VirtualMemoryContainer(size_t max_elements) {
        num_elements = max_elements;
        container = (ElementType*) mmap(        // allocate space in virtual memory
            nullptr,                            // let the kernel choose the location
            sizeof(ElementType) * max_elements, // amount of space being allocated
            PROT_NONE,                          // must be set to PROT_NONE, otherwise it errors out
            MAP_PRIVATE | MAP_ANONYMOUS,        // mapping is private to process, no underlying file
            -1,                                 // no underlying file => file descriptor set to -1
            0                                   // no offset
        );
        if (container == (ElementType*) -1) {
            std::string msg = "[VirtualMemoryContainer] Failed to allocate virtual memory (errno = " + std::to_string(errno) + ")";
            throw std::runtime_error(msg);
        }

        const size_t block_size = 1048576; // 1 GB -> 1.048.576 B
        int64_t remaining = sizeof(ElementType) * max_elements;
        uint8_t* head = (uint8_t *) container; // fix for -Wpointer-arith on void*

        while (remaining > 0) {
            int ret = mprotect(                             // change read and write permissions
                head,                                       // allocated space
                std::min((int64_t)block_size, remaining),   // size of block to be changed
                PROT_READ | PROT_WRITE                      // desired permissions
            );
            if (ret != 0) {
                std::string msg = "[VirtualMemoryContainer] Failed to set permissions (errno = " + std::to_string(errno) + ")";
                throw std::runtime_error(msg);
            }
            head += block_size;
            remaining -= block_size;
        }
    }

    // cannot be copied: there's no mechanism keeping track of actual space usage
    VirtualMemoryContainer(VirtualMemoryContainer& other) = delete;

    VirtualMemoryContainer(VirtualMemoryContainer&& other) {
        num_elements = other.num_elements;
        other.num_elements = 0;
        container = other.container;
        other.container = nullptr;
    }

    ~VirtualMemoryContainer() {
        if (container != nullptr) {
            int ret = munmap(container, sizeof(ElementType) * num_elements);
            if (ret != 0) {
                std::string msg = "[VirtualMemoryContainer] Failed to deallocate virtual memory (errno = " + std::to_string(errno) + ")";
                std::cerr << msg << std::endl;
                // throwing exceptions requires `noexcept(false)`, which requires all subsequent destructors to be declared noexcept
                // throw std::runtime_error(msg);
            }
        }
    }

public:
    inline ElementType& operator[](size_t idx) {
        return container[idx];
    }

    inline const ElementType& operator[](size_t idx) const {
        return container[idx];
    }

    inline size_t size() const {
        return num_elements;
    }
}; // class VirtualMemoryContainer

} // namespace vmc
