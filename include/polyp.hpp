#pragma once

#include <array>
#include <atomic>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <iterator>
#include <limits>
#include <mutex>
#include <shared_mutex>
#include <stack>
#include <stdexcept>
#include <string>
#include <vector>

#include "vmc.hpp"


namespace polyp {

/**
 * @brief Class that represents the Euclidean distance (L^2 norm).
 *
 * Euclidean distance or norm is defined as a positive square root of sum of vector's components.
 * This class represents this norm and provides functions to calculate the norm of a given vector.
 *
 * @tparam DIM vector size
 * @tparam Scalar scalar value type
 * @tparam Point vector value type
 * @tparam DT distance value type
 */
template <int DIM, typename Scalar = double, class Point = std::array<Scalar, DIM>, typename DT = Scalar>
class Euclidean {
public:
    /**
     * @brief Returns the Euclidean distance between two given vectors.
     *
     * Computes @f$\left(\sum_{i=0}^{\text{DIM}-1}(b[i] - a[i])^2\right)^{1/2}@f$.
     *
     * @warning To check if two vectors are close, use 'less' or 'leq' instead (performance).
     *
     * @param a first vector
     * @param b second vector
     * @return DT distance between given vectors
     */
    static DT dist(const Point &a, const Point &b) {
        DT temp = 0, acc = 0;
        for (int i = 0; i < DIM; i++) {
            temp = b[i] - a[i];
            acc += temp * temp;
        }
        return std::sqrt(acc);
    }

    /**
     * @brief Returns the Euclidean norm of the given vector.
     *
     * @warning To compare the vector size to some value, use 'less' or 'leq' instead (performance).
     *
     * @param a given vector
     * @return DT norm of the given vector
     */
    static DT dist(const Point &a) {
        DT acc = 0;
        for (int i = 0; i < DIM; i++) {
            acc += a[i] * a[i];
        }
        return std::sqrt(acc);
    }

    /**
     * @brief Check whether the distance between two vectors is smaller than or equal to some value.
     *
     * @param a first vector
     * @param b second vector
     * @param eps reference value
     * @return true if the distance between 'a' and 'b' is smaller than or equal to 'eps'
     * @return false if the distance between 'a' and 'b' is larger than 'eps'
     */
    static bool leq(const Point &a, const Point &b, const DT eps) {
        DT temp = 0, acc = 0;
        for (int i = 0; i < DIM; i++) {
            temp = b[i] - a[i];
            acc += temp * temp;
        }
        return acc <= eps * eps;
    }

    /**
     * @brief Check whether the vector norm is smaller than or equal to some value.
     *
     * @param a given vector
     * @param eps reference value
     * @return true if the vector norm is smaller than or equal to 'eps'
     * @return false if the vector norm is larger than 'eps'
     */
    static bool leq(const Point &a, const DT eps) {
        DT acc = 0;
        for (int i = 0; i < DIM; i++) {
            acc += a[i] * a[i];
        }
        return acc <= eps * eps;
    }

    /**
     * @brief Check whether the distance between two vectors is smaller than some value.
     *
     * @param a first vector
     * @param b second vector
     * @param eps reference value
     * @return true if the distance between 'a' and 'b' is smaller than 'eps'
     * @return false if the distance between 'a' and 'b' is larger than or equal to 'eps'
     */
    static bool less(const Point &a, const Point &b, DT const eps) {
        DT temp = 0, acc = 0;
        for (int i = 0; i < DIM; i++) {
            temp = b[i] - a[i];
            acc += temp * temp;
        }
        return acc < eps * eps;
    }

    /**
     * @brief Check whether the vector norm is smaller than some value.
     *
     * @param a given vector
     * @param eps reference value
     * @return true if the vector norm is smaller than 'eps'
     * @return false if the vector norm is larger than or equal to 'eps'
     */
    static bool less(const Point &a, const DT eps) {
        DT acc = 0;
        for (int i = 0; i < DIM; i++) {
            acc += a[i] * a[i];
        }
        return acc < eps * eps;
    }

    /**
     * @brief Check whether the first vector is closer to reference that the second
     *
     * @param center reference vector
     * @param a first vector
     * @param b second vector
     * @return true if the first vector is closer
     * @return false if the second vector is closer or if they are equally far from the reference
     */
    static bool less(const Point &center, const Point &a, const Point &b) {
        DT temp, acc_1 = 0, acc_2 = 0;
        for (int i = 0; i < DIM; i++) {
            temp = center[i] - a[i];
            acc_1 += temp * temp;
            temp = center[i] - b[i];
            acc_2 += temp * temp;
        }
        return acc_1 < acc_2;
    }
}; // class Euclidean

/**
 * @brief Class with type definitions needed to configure the Polyp indexing structure.
 *
 * Types and values in this class are sensible defaults, but can be modified by extending this class.
 *
 * @tparam dimension_ dimensionality of input data
 */
template <int dimension_>
struct DefaultPolypTraits {
    // scalar type
    typedef double Scalar;

    // dimensionality of vector space
    static constexpr int dimension = dimension_;

    // size of leaves
    static constexpr int leaf_size = 40;

    // type of a single point in vector space
    typedef typename std::array<Scalar, dimension> Point;

    // type of the point container
    typedef typename std::vector<Point> VectorOfPoints;

    // function which extracts separate components of a point
    // must be defined for idx from 0 to dimension - 1 for any Point p!
    static inline Scalar getComponent(const Point &p, const int idx) {
        return p[idx];
    }
};



/**
 * @brief Space-partitioning structure with arbitrary dimensionality.
 *
 * This structure supports data of arbitrary dimensionality.
 * The operating principle is similar to quadtrees and octrees, but generalized to more dimensions.
 * Inserted points are stored inside this structure and have to be extracted.
 *
 * @tparam Traits configuration object used to modify the tree (see DefaultPolypTraits class)
 */
template <typename Traits = DefaultPolypTraits<2>>
class Polyp {

private:
    // >>---- internal type definitions ----<<

    typedef typename Traits::Scalar Scalar;
    static constexpr int DIM = Traits::dimension;
    static constexpr int _LEAF_SIZE = Traits::leaf_size;
    typedef typename Traits::Point Point;
    typedef typename Traits::VectorOfPoints VectorOfPoints;
    template <typename T> using TSContainer = vmc::VirtualMemoryContainer<T>;
    typedef Polyp<Traits> Tree;

    // allows PolypDebugger<T> to access the tree's private fields
    template <typename T> friend class PolypDebugger;
    // allows PolypIterator<T> to access the tree's private fields
    friend class PolypIterator;

    // internal point representation
    typedef typename std::array<Scalar, DIM> InternalPoint;

    // distance value type
    typedef Scalar DT;
    // distance calculator type
    typedef Euclidean<DIM, Scalar, InternalPoint> Distance;

    // number of children of each internal node
    const static int CHILDREN = 1 << DIM;
    // zero-th node with 'internal' flag set
    const static uint32_t ROOT_NODE = 0x8000'0000u;
    const static uint32_t INTERNAL_FLAG = ROOT_NODE;
    const static uint32_t EMPTY_LEAF = 0u;

    // internal node representation
    struct Node {
        uint32_t children[CHILDREN];
        mutable std::shared_mutex mutexes[CHILDREN];
    };

    // leaf node representation
    struct Leaf {
        uint32_t children[_LEAF_SIZE];
    };

    // utility functions
    static inline bool isEmpty(const uint32_t i) { return i == EMPTY_LEAF; }
    static inline bool isLeaf(const uint32_t i) { return (i & INTERNAL_FLAG) == 0; }
    static inline uint32_t setInternalFlag(const uint32_t i) { return i | INTERNAL_FLAG; }
    static inline uint32_t unsetInternalFlag(const uint32_t i) { return i & (~INTERNAL_FLAG); }



    // >>---- helper functions for vector work ----<<
    // All functions opernate in-place.

    /**
     * @brief Add second vector to the first.
     *
     * @param a target vector
     * @param b vector to add to target
     */
    static inline void add(InternalPoint &a, const InternalPoint &b) {
        for (int i = 0; i < DIM; i++) {
            a[i] += b[i];
        }
    }

    /**
     * @brief Add scaled second vector to the first.
     *
     * @param a target vector
     * @param b vector to add to target
     * @param alpha scaling factor
     */
    static inline void add_scaled(InternalPoint &a, const InternalPoint &b, const Scalar alpha) {
        for (int i = 0; i < DIM; i++) {
            a[i] += alpha * b[i];
        }
    }

    /**
     * @brief Subtract second vector from the first.
     *
     * @param a target vector
     * @param b vector to subtract from target
     */
    static inline void sub(InternalPoint &a, const InternalPoint &b) {
        for (int i = 0; i < DIM; i++) {
            a[i] -= b[i];
        }
    }

    /**
     * @brief Subtract scaled second vector from the first.
     *
     * @param a target vector
     * @param b vector to subtract from target
     * @param alpha scaling factor
     */
    static inline void sub_scaled(InternalPoint &a, const InternalPoint &b, const Scalar alpha) {
        for (int i = 0; i < DIM; i++) {
            a[i] -= alpha * b[i];
        }
    }

    /**
     * @brief Multiply vector by a scalar.
     *
     * @param a target vector
     * @param alpha scaling factor
     */
    static inline void scalar_mult(InternalPoint &a, const Scalar alpha) {
        for (int i = 0; i < DIM; i++) {
            a[i] *= alpha;
        }
    }

    /**
     * @brief Add components of the second vector to the first, as defined by the mask.
     *
     * For the explanation of how the mask works see 'quadrant' function.
     *
     * @param a
     * @param b
     * @param mask
     */
    static inline void masked_add(InternalPoint &a, const InternalPoint &b, int mask) {
        for (int i = DIM - 1; i >= 0; i--) {
            a[i] += (mask & 0x0000'0001) * b[i];
            mask >>= 1;
        }
    }

    /**
     * @brief Determines the center and halves of widths of the bounding box defined by 'min' and
     *  'max'.
     *
     * The bounding box is defined as space between min[i] and max[i] in each dimension i between 0 and DIM - 1.
     * For each dimension i the following must hold: max[i] >= min[i].
     *
     * @param[in] min minimum values of the bounding box
     * @param[in] max maximum values of the bounding box
     * @param[out] center center of the bounding box
     * @param[out] hw halves of the widths of the bounding box
     */
    static inline void determine_center_and_halfwidth(const InternalPoint &min, const InternalPoint &max, InternalPoint &center, InternalPoint &hw) {
        for (int i = 0; i < DIM; i++) {
            center[i] = (min[i] + max[i]) / 2;
            hw[i] = (max[i] - min[i]) / 2;
        }
    }



    // >>---- data storage ----<<

    // internal node storage
    TSContainer<Node> nodes;
    // leaf node storage
    TSContainer<Leaf> leaves;
    // element storage
    TSContainer<Point> elements;
    // monotonic atomic counters
    std::atomic_uint32_t nNodes, nLeaves, nElements;
    // atomic counter of elements contained in the tree
    std::atomic_uint32_t elementCount = 0;

    /**
     * @brief Bounding box data.
     * In i-th dimension tree spans the space between bbmin[i] and bbmax[i].
     *
     */
    InternalPoint bbmin, bbmax;

    // debugging variables
    bool debug = false;
    bool printCollisions = false;
    uint32_t collisionCount = 0;



    /**
     * @brief Iterator over the points contained in the tree.
     *
     */
    class PolypIterator {

    private:
        // reference to the tree
        const Tree& tree;
        // point type
        typedef typename Tree::Point Point;

        // path from the root to the leaf containing the point
        // empty path signifies an invalid iterator
        std::stack<std::array<uint32_t, 2>> tree_path;
        // index of the leaf containing the point
        uint32_t leaf_idx = 0;
        // index of the point inside the leaf
        uint32_t leaf_loc = 0;
        // shared lock of the leaf
        std::shared_lock<std::shared_mutex> leaf_lock;

        friend Polyp;

    public:
        PolypIterator(const Tree& _tree) : tree(_tree) {}

        PolypIterator(PolypIterator& other) : tree(other.tree), tree_path(other.tree_path) {
            leaf_idx = other.leaf_idx;
            leaf_loc = other.leaf_loc;
            if (other.leaf_lock.mutex()) {
                // lock the same mutex
                leaf_lock = std::shared_lock(*other.leaf_lock.mutex());
            }
        }

        // ++iterator
        PolypIterator& operator++() {
            if (tree_path.top()[0] == ROOT_NODE && tree_path.top()[1] >= CHILDREN) {
                // already over the end
                return *this;
            }

            do {
                ++leaf_loc;
                if (leaf_loc >= _LEAF_SIZE) {
                    // move to next leaf
                    tree_path.top()[1]++;

                    while (tree_path.top()[1] >= CHILDREN && tree_path.top()[0] != ROOT_NODE) {
                        // traversed subtree, move to next one
                        tree_path.pop();
                        tree_path.top()[1]++;
                    }

                    if (tree_path.top()[0] == ROOT_NODE && tree_path.top()[1] >= CHILDREN) {
                        // traversed whole tree, set end state
                        leaf_idx = 0;
                        leaf_loc = 0;
                        // release shared lock
                        leaf_lock = std::shared_lock<std::shared_mutex>();
                        return *this;
                    }

                    while (true) {
                        uint32_t branch = tree.nodes[unsetInternalFlag(tree_path.top()[0])]
                            .children[tree_path.top()[1]];
                        if (Tree::isLeaf(branch)) {
                            // branch is a leaf, set new position
                            leaf_idx = branch;
                            leaf_loc = 0;
                            leaf_lock = std::shared_lock(
                                tree
                                .nodes[unsetInternalFlag(tree_path.top()[0])]
                                .mutexes[tree_path.top()[1]]
                            );
                            break;
                        }
                        // branch is an internal node, move deeper
                        tree_path.push({branch, 0U});
                    }
                }
            } while (tree.leaves[leaf_idx].children[leaf_loc] == 0);

            // iterator points to a valid position
            return *this;
        }

        // iterator++
        PolypIterator operator++(int) {
            PolypIterator ret = *this;
            this->operator++();
            return ret;
        }

        // *iterator
        const Point& operator*() {
            if (leaf_idx == 0) {
                throw std::runtime_error("[PolypIterator] dereferenced invalid iterator!");
            }
            return tree.elements[tree.leaves[leaf_idx].children[leaf_loc]];
        }

        // iter1 == iter2
        bool operator==(PolypIterator other) {
            if (leaf_idx > 0) {
                if (leaf_idx == other.leaf_idx) {
                    return leaf_loc == other.leaf_loc;
                }
                return false;
            }
            if (other.leaf_idx == 0) {
                return true;
            }
            return false;
        }

        // iter1 != iter2
        inline bool operator!=(PolypIterator other) {
            return !operator==(other);
        }

        // iterator traits
        using iterator_category = std::input_iterator_tag;
        using difference_type = void;
        using value_type = void;
        using pointer = void;
        using reference = void;
    }; // class PolypIterator



    // >>---- internal helper functions ----<<

    /**
     * @brief Calculates the quadrant of the vector 'pos' relative to the 'center'.
     *
     * Returns an integer that represents a DIM-dimensional bit vector.
     * A bit in position i is set to 1 only if the given vector's i-th coordinate is larger than the center's.
     * The vector is zero-padded and right-aligned (LSB of the returned integer corresponds to the (DIM-1)-th coordinate)
     *
     * @param pos given vector
     * @param center coordinate center
     * @return int a DIM-dimensional bit vector
     */
    static inline int quadrant(const InternalPoint &pos, const InternalPoint &center) {
        int ret = 0;
        for (int i = 0; i < DIM; i++) {
            ret <<= 1;
            if (pos[i] > center[i]) ret++;
        }
        return ret;
    }

    /**
     * @brief Calculates the quadrant of the vector 'pos' relative to the center of the quadrant, defined by 'center', 'hw' and 'quad'.
     *
     * @param pos given vector
     * @param center coordinate center
     * @param hw halves of the widths of the bounding box
     * @param quad quadrant index
     * @return int a DIM-dimensional bit vector
     */
    static inline int masked_quadrant(const InternalPoint &pos, const InternalPoint &center, const InternalPoint& hw, const int quad) {
        int ret = 0;
        for (int i = 0; i < DIM; i++) {
            ret <<= 1;
            Scalar newCenter = center[i] - (0.5 * hw[i]);
            if (quad & (0x0000'0001u << (DIM - 1 - i))) newCenter += hw[i];
            if (pos[i] > newCenter) ret++;
        }
        return ret;
    }

    /**
     * @brief Calculates the positive distances of a point from a tree.
     *
     * Each coordinate of the returned value is positive iff the point lies outside of the bounding
     *  box of the tree, zero otherwise.
     *
     * @param[out] ret returned distances
     * @param pos given point
     * @param center coordinate center
     * @param hw halves of the bounding box widths in each dimension
     * @param quadrant bit vector that describes the given point's position relative to the center
     */
    static inline void distances(InternalPoint &ret, const InternalPoint &pos, const InternalPoint &center, const InternalPoint &hw, int quadrant) {
        for (int i = DIM - 1; i >= 0; i--) {
            if (quadrant & 0x0000'0001) {
                // coordinate is larger than center's
                ret[i] = pos[i] - center[i] - hw[i];
            } else {
                // coordinate is smaller than or equal to center's
                ret[i] = center[i] - hw[i] - pos[i];
            }
            // negative value implies point lies inside the tree, so we ignore it
            if (ret[i] < 0) ret[i] = 0;
            quadrant >>= 1;
        }
    }

    /**
     * @brief Safely allocates a spot for the new element in the element list and inserts it.
     *
     * @param pos new element
     * @return uint32_t element's index in the storage structure
     */
    inline uint32_t safely_insert_element(const Point &pos) {
        uint32_t ref = nElements++;
        if (ref >= elements.size()) {
            // over the limit!
            if (debug) std::cout << "Elements full!";
            return ROOT_NODE;
        }
        elementCount++;
        elements[ref] = pos;
        return ref;
    }

    /**
     * @brief Safely allocates a spot for the new node in the node list
     *
     * @return uint32_t node's index in the storage structure if allocation succeeded, ROOT_NODE if not
     */
    inline uint32_t safely_reserve_node() {
        uint32_t ref = nNodes++;
        if (ref >= nodes.size()) {
            // over the limit!
            if (debug) std::cout << "Nodes full!";
            return ROOT_NODE;
        }
        return ref;
    }

    /**
     * @brief Safely allocates a spot for the new leaf in the leaf list
     *
     * @return uint32_t leaf's index in the storage structure if allocation succeeded, ROOT_NODE if not
     */
    inline uint32_t safely_reserve_leaf() {
        uint32_t ref = nLeaves++;
        if (ref >= leaves.size()) {
            // over the limit!
            if (debug) std::cout << "Leaves full!";
            return ROOT_NODE;
        }
        return ref;
    }

    /**
     * @brief Copies the location data from a user-provided vector into an internal representation.
     *
     * @param source user-provided vector
     * @param destination internal representation of the vector
     */
    static inline void pointToInternalPoint(const Point &source, InternalPoint &destination) {
        for (int i = 0; i < DIM; i++) {
            destination[i] = Traits::getComponent(source, i);
        }
    }

    /**
     * @brief Sets the number of elements to maximum if the passed number is 0.
     *
     * @param n passed number of elements
     * @return uint32_t final number of elements
     */
    static inline uint32_t max_elements(uint32_t n) {
        return n == 0 ? 0x8000'0000UL : n;
    }

    /**
     * @brief Calculates the number of leaves and internal nodes based on the number of elements.
     *
     * @param n passed number of elements
     * @return uint32_t final number of leaves and internal nodes
     */
    static inline uint32_t max_leaves(uint32_t n) {
        uint64_t ret = max_elements(n);
        // multiply by CHILDREN for the worst case: each final internal node holds _LEAF_SIZE + 1 elements in its leaves
        // divide by _LEAF_SIZE for the best case: each leaf is full
        ret = CHILDREN * ret / _LEAF_SIZE;
        // limit the number of leaves to the 31-bit limit (MSB is a flag)
        ret = ret > 0x8000'0000UL ? 0x8000'0000UL : ret;
        // tree starts with CHILDREN leaves, so this is a hard minimum
        ret = ret < CHILDREN ? CHILDREN : ret;
        return ret;
    }



    // >>---- main functions ----<<

    /**
     * @brief Check whether an element can be inserted into the subtree.
     *
     * @param node index of internal node representing a subtree in which the possible insertion location should be looked for
     * @param pos given element
     * @param eps vicinity of the given element that should not contain any other element
     * @param center center of the subtree
     * @param halfwidth halves of widths of the subtree's bounding box
     * @param ignoring optional index of an element to be ignored in the procedure
     * @return true if no element in 'node' is in 'eps'-vicinity of 'pos'
     * @return false if there exists an element in 'node' that is in 'eps'-vicinity of 'pos'
     */
    bool check_node(const uint32_t node, const InternalPoint &pos, const DT eps, const InternalPoint &center, const InternalPoint &halfwidth, const uint32_t ignoring = 0u) {
        // check if leaf was given
        if (isLeaf(node)) {
            throw std::runtime_error("[Polyp] Expected an internal node!");
        }

        // WARNING: this procedure assumes the point on a hyperplane nearest to a given point is the orthographic projection of the given point on the hyperplane
        // determine quadrant
        int quad = quadrant(pos, center);
        InternalPoint dists;
        // determine distances from tree
        distances(dists, pos, center, halfwidth, quad);

        // is the tree close enough?
        if (Distance::leq(dists, eps)) {
            // tree is close enough, recurse
            InternalPoint new_center, new_hw = halfwidth;
            scalar_mult(new_hw, 0.5);
            // first bit is a subtree flag
            Node &tree = nodes[unsetInternalFlag(node)];

            for (int i = 0; i < CHILDREN; i++) {
                // prioritize the quadrant where child resides
                uint32_t child_idx = (i + quad) & (CHILDREN - 1);
                if (isLeaf(tree.children[child_idx])) {
                    if (!check_leaf(node, child_idx, pos, eps, center, halfwidth, ignoring)) {
                        // there's an element in i-th leaf that's in the eps-vicinity
                        return false;
                    }
                } else {
                    new_center = center;
                    sub(new_center, new_hw);
                    masked_add(new_center, halfwidth, child_idx);
                    if (!check_node(tree.children[child_idx], pos, eps, new_center, new_hw, ignoring)) {
                        // there's an element in i-th subtree that's in the eps-vicinity
                        return false;
                    }
                }
            }

            // all subtrees have returned true, therefore there's no element in eps-vicinity
            return true;
        }

        // tree is too far away from the point, no point inside is in eps-vicinity
        return true;
    }

    /**
     * @brief Check whether an element can be inserted into the leaf.
     *
     * @param node leaf's parent node
     * @param leaf_quadrant leaf's quadrant in the parent subtree
     * @param pos given element
     * @param eps vicinity of the given element that should not contain any other element
     * @param center center of the parent node
     * @param halfwidth halves of widths of the parent's bounding box
     * @param ignoring optional index of an element to be ignored in the procedure
     * @return true if no element in 'node' is in 'eps'-vicinity of 'pos'
     * @return false if there exists an element in 'node' that is in 'eps'-vicinity of 'pos'
     */
    bool check_leaf(const uint32_t node, const uint32_t leaf_quadrant, const InternalPoint &pos, const DT eps, const InternalPoint &center, const InternalPoint &halfwidth, const uint32_t ignoring = 0u) {
        // check if leaf was given
        if (isLeaf(node)) {
            throw std::runtime_error("[Polyp] Expected an internal node!");
        }

        {
            Node &parent = nodes[unsetInternalFlag(node)];
            std::shared_lock lock(parent.mutexes[leaf_quadrant]);
            if (isLeaf(parent.children[leaf_quadrant])) {
                if (isEmpty(parent.children[leaf_quadrant])) {
                    // invalid state -> throw error
                    throw std::runtime_error("[Polyp] Invalid tree state - internal node contains a reference to no leaf!");
                }

                // return false iff an element in the leaf is less than or equal to eps away
                InternalPoint node_internal;
                Leaf &leaf = leaves[parent.children[leaf_quadrant]];

                // check all elements in the leaf
                for (int i = 0; i < _LEAF_SIZE; i++) {
                    const uint32_t &child = leaf.children[i];
                    if (child != 0 && child != ignoring) {
                        pointToInternalPoint(elements[child], node_internal);
                        if (Distance::leq(node_internal, pos, eps)) {
                            // too close!
                            return false;
                        }
                    }
                }

                // no element in the leaf was in eps-vicinity
                return true;
            }
        }

        // not a leaf => some other thread branched out the leaf before mutex was locked
        InternalPoint new_center = center, new_hw = halfwidth;
        scalar_mult(new_hw, 0.5);
        sub(new_center, new_hw);
        masked_add(new_center, halfwidth, leaf_quadrant);
        return check_node(nodes[unsetInternalFlag(node)].children[leaf_quadrant], pos, eps, new_center, new_hw, ignoring);
    }

    /**
     * @brief Removes the element from the tree.
     *
     * @warning Does not clean-up the tree structure, only swaps the element's index with an empty one!
     * @warning Relies on other functions of the tree to only push the element deeper towards the leaves.
     *
     * @param element index of the element to be removed
     * @param pos position of the element in space
     * @param node_idx index of internal node the element should be located in
     * @param leaf_quadrant quadrant of leaf the element should be located in
     * @param center center of the subtree
     * @param halfwidth halves of widths of the subtree's bounding box
     */
    void remove_element(const uint32_t element, const InternalPoint &pos, uint32_t node_idx, uint32_t leaf_quadrant, InternalPoint &center, InternalPoint &halfwidth) {
        while (true) {
            Node &node = nodes[unsetInternalFlag(node_idx)];
            std::unique_lock lock(node.mutexes[leaf_quadrant]);

            // check if another thread branched already
            if (!isLeaf(node.children[leaf_quadrant])) {
                node_idx = node.children[leaf_quadrant];
                InternalPoint new_hw = halfwidth;
                scalar_mult(new_hw, 0.5);
                sub(center, new_hw);
                masked_add(center, halfwidth, leaf_quadrant);
                halfwidth = new_hw;
                leaf_quadrant = quadrant(pos, center);
                continue;
            }

            // given node is a leaf, mutex is locked
            Leaf &leaf = leaves[node.children[leaf_quadrant]];
            for (int i = 0; i < _LEAF_SIZE; i++) {
                if (leaf.children[i] == element) {
                    // found element to remove
                    leaf.children[i] = 0;
                    elementCount--;
                    return;
                }
            }

            // leaf was checked, but the element to remove was not found
            throw std::runtime_error("[Polyp] tried to remove nonexistent element!");
        }
    }

    /**
     * @brief Branches out a full leaf into a new subtree.
     *
     * @param element element to be inserted into the new subtree
     * @param pos position of the element in space
     * @param leaf_idx index of the leaf the element should be located in
     * @param center center of the newly created subtree
     * @param halfwidth halves of widths of the subtree's bounding box
     * @return uint32_t index of the internal node representing the newly created subtree, or ROOT_NODE if branching failed
     */
    uint32_t branch_leaf_and_insert(const uint32_t element, const InternalPoint &pos, const uint32_t leaf_idx, InternalPoint center, InternalPoint halfwidth) {
        // reserve new node
        uint32_t new_node_idx = safely_reserve_node();
        if (new_node_idx == ROOT_NODE) {
            // reserving the node failed
            return ROOT_NODE;
        }
        Node &new_node = nodes[new_node_idx];
        Leaf &old_leaf = leaves[leaf_idx];

        // reuse old leaf
        new_node.children[0] = leaf_idx;

        // fill out other leaves
        for (int i = 1; i < CHILDREN; i++) {
            new_node.children[i] = safely_reserve_leaf();
            if (new_node.children[i] == ROOT_NODE) {
                // reserving the leaf failed
                return ROOT_NODE;
            }
        }

        // move each existing element in appropriate leaf
        // old leaf is full => all places contain an element
        for (int i = 0; i < _LEAF_SIZE; i++) {
            InternalPoint element_internal;
            pointToInternalPoint(elements[old_leaf.children[i]], element_internal);
            int quad = quadrant(element_internal, center);
            if (quad > 0) {
                // move element into appropriate leaf
                leaves[new_node.children[quad]].children[i] = old_leaf.children[i];
                // set empty element in old leaf
                old_leaf.children[i] = 0;
            }
        }

        // find empty spot for new element
        int empty = -1, quad = quadrant(pos, center);
        Leaf &new_leaf = leaves[new_node.children[quad]];
        for (int i = 0; i < _LEAF_SIZE; i++) {
            if (new_leaf.children[i] == 0) {
                // found an empty spot for inserting element into
                empty = i;
            }
        }

        if (empty < 0) {
            // recurse
            masked_add(center, halfwidth, quad);
            scalar_mult(halfwidth, 0.5);
            sub(center, halfwidth);

            uint32_t new_leaf = branch_leaf_and_insert(element, pos, new_node.children[quad],
                center, halfwidth);
            if (new_leaf == ROOT_NODE) {
                // branching failed
                return ROOT_NODE;
            }
            new_node.children[quad] = new_leaf;
        } else {
            // insert element into the empty spot
            new_leaf.children[empty] = element;
        }

        return setInternalFlag(new_node_idx);
    }

    /**
     * @brief Recursively fills up 'ret' with points from the tree.
     * Only used internally, see 'extract_points' for external use.
     *
     * @param ret container for the points to be inserted into
     * @param node subtree frow which the points should be extracted
     */
    void extract_from_subtree(VectorOfPoints &ret, const uint32_t node) const {
        if (isLeaf(node)) {
            const Leaf &leaf = leaves[node];
            for (int i = 0; i < _LEAF_SIZE; i++) {
                if (!isEmpty(leaf.children[i])) {
                    // node is a valid point => insert it
                    Point temp = elements[leaf.children[i]];
                    ret.push_back(temp);
                }
            }
        } else {
            // node is internal => recurse
            for (int i = 0; i < CHILDREN; i++) {
                extract_from_subtree(ret, nodes[unsetInternalFlag(node)].children[i]);
            }
        }
    }

    /**
     * @brief Inserts the candidate into the container if possible
     *
     * @param k maximum number of elements in the container
     * @param reference reference point whose neighbors are being looked for
     * @param candidate point that is a candidate for a returned neighbor
     * @param ret_indices container of returned indices
     * @param ret_dists container of returned distances
     */
    void nearest_insert_into_vector(const unsigned int k, const InternalPoint &reference, const uint32_t candidate, std::vector<size_t> &ret_indices, std::vector<DT> &ret_dists) const {
        // calculate local variables
        unsigned int n = ret_indices.size();
        InternalPoint candidate_point;
        pointToInternalPoint(elements[candidate], candidate_point);

        for (unsigned int i = 0; i < k; i++) {
            // i == n => first empty spot => insert in any case
            // closer to reference than current => insert before this element
            if (i == n || Distance::less(reference, candidate_point, ret_dists[i])) {
                ret_indices.insert(ret_indices.begin() + i, candidate);
                ret_dists.insert(ret_dists.begin() + i, Distance::dist(reference, candidate_point));
                break;
            }
        }

        // cleanup
        if (ret_indices.size() > k) {
            ret_indices.resize(k);
            ret_dists.resize(k);
        }
    }

    /**
     * @brief Inserts the candidate into the container if possible
     *
     * @tparam k maximum number of elements in the container
     * @param reference reference point whose neighbors are being looked for
     * @param candidate point that is a candidate for a returned neighbor
     * @param ret_indices container of returned indices
     * @param ret_dists container of returned distances
     */
    template <unsigned int k>
    void nearest_insert_into_array(const InternalPoint &reference, const uint32_t candidate, std::array<size_t, k> &ret_indices, std::array<DT, k> &ret_dists) const {
        // calculate local variables
        InternalPoint candidate_point;
        pointToInternalPoint(elements[candidate], candidate_point);

        for (unsigned int i = 0; i < k; i++) {
            // i == n => first empty spot => insert in any case
            // closer to reference than current => insert before this element
            if (Distance::less(reference, candidate_point, ret_dists[i])) {
                for (unsigned int j = k - 1; j > i; j--) {
                    ret_indices[j] = ret_indices[j-1];
                    ret_dists[j] = ret_dists[j-1];
                }
                ret_indices[i] = candidate;
                ret_dists[i] = Distance::dist(reference, candidate_point);
                break;
            }
        }
    }

    /**
     * @brief Returns the distance of the farthest neighbor in the container.
     *  If the container is not full, returns infinity.
     *
     * @param k desired number of neighbors
     * @param ret_dists container of distances
     * @return DT worst distance in 'ret'
     */
    static DT nearest_worst_distance(const unsigned int k, std::vector<DT> &ret_dists) {
        // if there's still empty space => return infinity
        if (ret_dists.size() < k) return std::numeric_limits<DT>::infinity();

        // all space is occupied => return the distance of the last (furthest) element
        return ret_dists[k-1];
    }

    /**
     * @brief Returns the distance of the farthest neighbor in the container.
     *  If the container is not full, returns infinity.
     *
     * @tparam k desired number of neighbors
     * @param ret_dists container of distances
     * @return DT worst distance in 'ret'
     */
    template <unsigned int k>
    static inline DT nearest_worst_distance(std::array<DT, k> &ret_dists) {
        // return the distance of the last (furthest) element
        return ret_dists[k-1];
    }

    /**
     * @brief Look for neighbors of the reference element in subtree rooted in the given node.
     *
     * @param node_idx root of the subtree where neighbors are to be looked for - must be internal node!
     * @param center center of the bounding box of the tree
     * @param hw halves of the widths of the bounding box
     * @param k desired number of neighbors
     * @param reference points whose neighbors are being looked for
     * @param ret_indices container of returned indices
     * @param ret_dists container of returned distances
     */
    void nearest_find(uint32_t node_idx, const InternalPoint &center, const InternalPoint &hw, const unsigned int k, const InternalPoint &reference, std::vector<size_t> &ret_indices, std::vector<DT> &ret_dists) const {
        // check if leaf was given
        if (isLeaf(node_idx)) {
            throw std::runtime_error("[Polyp] expected an internal node!");
        }

        // check all children
        const Node &node = nodes[unsetInternalFlag(node_idx)];
        InternalPoint new_center, new_hw = hw;
        scalar_mult(new_hw, 0.5);

        // calculate element's quadrant in the tree
        int child_quad = quadrant(reference, center);

        for (int i = 0; i < CHILDREN; i++) {
            // prioritize the quadrant where child resides
            uint32_t child = node.children[(i + child_quad) & (CHILDREN - 1)];

            // process whether the child is a leaf
            if (isLeaf(child)) {
                // child is leaf => lock, try inserting
                std::shared_lock lock(node.mutexes[i]);
                if (isLeaf(child)) {
                    if (isEmpty(child)) {
                        // empty leaf => error
                        throw std::runtime_error("[Polyp] found reference to no leaf!");
                    } else {
                        // nonempty leaf => try inserting
                        for (int j = 0; j < _LEAF_SIZE; j++) {
                            uint32_t curr_child = leaves[child].children[j];
                            if (!isEmpty(curr_child)) {
                                nearest_insert_into_vector(k, reference, curr_child, ret_indices, ret_dists);
                            }
                        }
                    }
                    continue;
                }
            }

            // child is an internal node, calculate child's center
            new_center = center;
            sub(new_center, new_hw);
            masked_add(new_center, hw, i);

            // determine whether the subtree is close enough to warrant checking
            // WARNING: this procedure assumes the point on a hyperplane nearest to a given point is the orthographic projection of the given point on the hyperplane
            InternalPoint dists;
            distances(dists, reference, new_center, new_hw, quadrant(reference, new_center));

            // determine the current worst distance
            DT worst_distance = nearest_worst_distance(k, ret_dists);

            if (Distance::leq(dists, worst_distance)) {
                // tree is close enough, look deeper
                nearest_find(child, new_center, new_hw, k, reference, ret_indices, ret_dists);
            }
        }
    }

    /**
     * @brief Look for neighbors of the reference element in subtree rooted in the given node.
     *
     * @tparam k desired number of neighbors
     * @param node_idx root of the subtree where neighbors are to be looked for - must be internal node!
     * @param center center of the bounding box of the tree
     * @param hw halves of the widths of the bounding box
     * @param reference points whose neighbors are being looked for
     * @param ret_indices container of returned indices
     * @param ret_dists container of returned distances
     */
    template <unsigned int k>
    void nearest_find(uint32_t node_idx, const InternalPoint &center, const InternalPoint &hw, const InternalPoint &reference, std::array<size_t, k> &ret_indices, std::array<DT, k> &ret_dists) const {
        // check if leaf was given
        if (isLeaf(node_idx)) {
            throw std::runtime_error("[Polyp] expected an internal node!");
        }

        // check all children
        const Node &node = nodes[unsetInternalFlag(node_idx)];
        InternalPoint new_center, new_hw = hw;
        scalar_mult(new_hw, 0.5);

        // calculate element's quadrant in the tree
        int child_quad = quadrant(reference, center);

        for (int i = 0; i < CHILDREN; i++) {
            // prioritize the quadrant where child resides
            uint32_t child = node.children[(i + child_quad) & (CHILDREN - 1)];

            // process whether the child is a leaf
            if (isLeaf(child)) {
                // child is leaf => lock, try inserting
                std::shared_lock lock(node.mutexes[i]);
                if (isLeaf(child)) {
                    if (isEmpty(child)) {
                        // empty leaf => error
                        throw std::runtime_error("[Polyp] found reference to no leaf!");
                    } else {
                        // nonempty leaf => try inserting
                        for (int j = 0; j < _LEAF_SIZE; j++) {
                            uint32_t curr_child = leaves[child].children[j];
                            if (!isEmpty(curr_child)) {
                                nearest_insert_into_array<k>(reference, curr_child, ret_indices, ret_dists);
                            }
                        }
                    }
                    continue;
                }
            }

            // child is an internal node, calculate child's center
            new_center = center;
            sub(new_center, new_hw);
            masked_add(new_center, hw, i);

            // determine whether the subtree is close enough to warrant checking
            // WARNING: this procedure assumes the point on a hyperplane nearest to a given point is the orthographic projection of the given point on the hyperplane
            InternalPoint dists;
            distances(dists, reference, new_center, new_hw, quadrant(reference, new_center));

            // determine the current worst distance
            DT worst_distance = nearest_worst_distance<k>(ret_dists);

            if (Distance::leq(dists, worst_distance)) {
                // tree is close enough, look deeper
                nearest_find<k>(child, new_center, new_hw, reference, ret_indices, ret_dists);
            }
        }
    }

    /**
     * @brief Look for neighbors of the reference element in subtree rooted in the given node.
     *
     * @param node_idx root of the subtree where neighbors are to be looked for - must be internal node!
     * @param center center of the bounding box of the tree
     * @param hw halves of the widths of the bounding box
     * @param reference points whose neighbors are being looked for
     * @param ret_indices container of returned indices
     * @param ret_dists container of returned distances
     */
    void nearest_find(uint32_t node_idx, const InternalPoint &center, const InternalPoint &hw,
        const InternalPoint &reference, std::array<size_t, 1> &ret_indices,
        std::array<DT, 1> &ret_dists) const {
        // check if leaf was given
        if (isLeaf(node_idx)) {
            throw std::runtime_error("[Polyp] expected an internal node!");
        }

        // check all children
        const Node &node = nodes[unsetInternalFlag(node_idx)];
        InternalPoint new_center, new_hw = hw;
        scalar_mult(new_hw, 0.5);

        // calculate element's quadrant in the tree
        int child_quad = quadrant(reference, center);

        for (int i = 0; i < CHILDREN; i++) {
            // prioritize the quadrant where child resides
            uint32_t child = node.children[(i + child_quad) & (CHILDREN - 1)];

            // process whether the child is a leaf
            if (isLeaf(child)) {
                // child is leaf => lock, try inserting
                std::shared_lock lock(node.mutexes[i]);
                if (isLeaf(child)) {
                    if (isEmpty(child)) {
                        // empty leaf => error
                        throw std::runtime_error("[Polyp] found reference to no leaf!");
                    } else {
                        // nonempty leaf => try inserting
                        for (int j = 0; j < _LEAF_SIZE; j++) {
                            uint32_t curr_child = leaves[child].children[j];
                            if (!isEmpty(curr_child)) {
                                InternalPoint candidate_point;
                                pointToInternalPoint(elements[curr_child], candidate_point);
                                if (Distance::less(reference, candidate_point, ret_dists[0])) {
                                    ret_indices[i] = curr_child;
                                    ret_dists[i] = Distance::dist(reference, candidate_point);
                                }
                            }
                        }
                    }
                    continue;
                }
            }

            // child is an internal node, calculate child's center
            new_center = center;
            sub(new_center, new_hw);
            masked_add(new_center, hw, i);

            // determine whether the subtree is close enough to warrant checking
            // WARNING: this procedure assumes the point on a hyperplane nearest to a given point is the orthographic projection of the given point on the hyperplane
            InternalPoint dists;
            distances(dists, reference, new_center, new_hw, quadrant(reference, new_center));

            if (Distance::leq(dists, ret_dists[0])) {
                // tree is close enough, look deeper
                nearest_find(child, new_center, new_hw, reference, ret_indices, ret_dists);
            }
        }
    }


public:
    // >>---- public single-threaded interface ----<<
    // These methods may not be called by more than one thread simultaneously.
    // Furthermore, when one of these methods is being executed, no thread
    // may call into any multi-threaded method as well.

    /**
     * @brief Construct a new Polyp object.
     *
     * @param min minimum values of the bounding box
     * @param max maximum values of the bounding box
     * @param maxElements maximum number of elements to be inserted into the tree
     */
    Polyp(const Point &min, const Point &max, uint32_t maxElements = 0)
        : nodes(max_leaves(maxElements)),
        leaves(max_leaves(maxElements)),
        elements(max_elements(maxElements)) {
        // check bounding box
        for (int i = 0; i < DIM; i++) {
            if (Traits::getComponent(max, i) - Traits::getComponent(min, i) < 0) {
                throw std::runtime_error("[Polyp] bounding box max must not be smaller than min!");
            }
        }

        // fill bounding box
        pointToInternalPoint(min, bbmin);
        pointToInternalPoint(max, bbmax);

        // setup base structure: one internal node pointing to CHILDREN leaves
        for (int i = 0; i < CHILDREN; i++) {
            nodes[0].children[i] = i + 1;
        }

        // node 0 is the base node
        nNodes = 1;
        // leaf 0 stands for "no leaf" -> invalid state
        nLeaves = CHILDREN + 1;
        // element 0 stands for "empty element", so we ignore it
        nElements = 1;
    }

    /**
     * @brief Construct a new Polyp object.
     *
     * @param min minimum value for all dimensions of the bounding box
     * @param max maximum value for all dimensions of the bounding box
     * @param maxElements maximum number of elements to be inserted into the tree
     */
    Polyp(Scalar min, Scalar max, uint32_t maxElements = 0)
        : nodes(max_leaves(maxElements)),
        leaves(max_leaves(maxElements)),
        elements(max_elements(maxElements)) {
        // check bounding box
        if (max - min < 0.) {
            throw std::runtime_error("[Polyp] bounding box max must not be smaller than min!");
        }

        // fill bounding box
        bbmin.fill(min);
        bbmax.fill(max);

        // setup base structure: one internal node pointing to CHILDREN leaves
        for (int i = 0; i < CHILDREN; i++) {
            nodes[0].children[i] = i + 1;
        }

        // node 0 is the base node
        nNodes = 1;
        // leaf 0 stands for "no leaf" -> invalid state
        nLeaves = CHILDREN + 1;
        // element 0 stands for "empty element", so we ignore it
        nElements = 1;
    }

    /**
     * @brief Copy constructor.
     *
     * @param other old tree with data to be copied from
     */
    Polyp(Polyp &other) = delete;

    /**
     * @brief Move constructor.
     *
     * @param other old tree with data to be moved from
     */
    Polyp(Polyp &&other)
        : nodes(std::move(other.nodes)),
        leaves(std::move(other.leaves)),
        elements(std::move(other.elements)) {
        nNodes = other.nNodes.load();
        nLeaves = other.nLeaves.load();
        nElements = other.nElements.load();
        elementCount = other.elementCount.load();
        bbmin = other.bbmin;
        bbmax = other.bbmax;
    }

    /**
     * @brief Returns a vector of points in the tree.
     *
     * @return VectorOfPoints vector of points
     */
    VectorOfPoints extract_points() const {
        // check whether the tree was initialized
        if (nodes.size() == 0) {
            throw std::runtime_error("[Polyp] tree was not initialized!");
        }

        VectorOfPoints ret;
        ret.reserve(nNodes);
        extract_from_subtree(ret, ROOT_NODE);
        ret.shrink_to_fit();
        return ret;
    }

    /**
     * @brief Returns a vector of points in the tree.
     *
     * @warning Deletes everything in the given vector!
     *
     * @param ret vector of points
     */
    void extract_points(VectorOfPoints &ret) const {
        // check whether the tree was initialized
        if (nodes.size() == 0) {
            throw std::runtime_error("[Polyp] tree was not initialized!");
        }

        ret.clear();
        ret.reserve(nNodes);
        extract_from_subtree(ret, ROOT_NODE);
        ret.shrink_to_fit();
    }


public:
    // >>---- public multi-threaded interface ----<< */
    // These methods can be called in a multi-threaded context,
    // by as many threads as needed simultaneously.

    /**
     * @brief Insert an element into the tree.
     *
     * @param x element to be inserted
     * @param eps vicinity of x that should not contain any other element
     * @return index of inserted element if insertion was possible
     * @return 0x80000000 (ROOT_NODE) if insertion failed
     */
    uint32_t insert(const Point &x, const DT eps = 0.) {
        // check whether the tree was initialized
        if (nodes.size() == 0) {
            throw std::runtime_error("[Polyp] tree was not initialized!");
        }

        // check if x lies inside the tree
        for (int i = 0; i < DIM; i++) {
            if (Traits::getComponent(x, i) > bbmax[i] || Traits::getComponent(x, i) < bbmin[i]) {
                if (debug) std::cout << "[Polyp] Point outside the tree!\n";
                return ROOT_NODE;
            }
        }

        // make a copy of x as an internal point
        InternalPoint x_internal;
        pointToInternalPoint(x, x_internal);

        // determine center, halfwidths of the bounding box
        // change during descent into the tree
        InternalPoint center, hw;
        determine_center_and_halfwidth(bbmin, bbmax, center, hw);

        // find leaf where insertion could be possible
        uint32_t parent_node_idx = ROOT_NODE;
        uint32_t leaf_quadrant = quadrant(x_internal, center);
        uint32_t x_idx = 0;

        while (true) {
            // check other quadrants for possible collisions
            InternalPoint new_hw = hw, new_center;
            scalar_mult(new_hw, 0.5);
            Node &parent = nodes[unsetInternalFlag(parent_node_idx)];

            for (uint32_t i = 0; i < CHILDREN; i++) {
                // ignore node's quadrant
                if (i == leaf_quadrant) continue;

                new_center = center;
                sub(new_center, new_hw);
                masked_add(new_center, hw, i);

                // use appropriate check depending on child's type
                if (isLeaf(parent.children[i])) {
                    if (!check_leaf(parent_node_idx, i, x_internal, eps, new_center, new_hw)) {
                        // there's an element in i-th subtree that's in the eps-vicinity
                        // => cannot insert the element
                        return ROOT_NODE;
                    }
                } else {
                    if (!check_node(parent.children[i], x_internal, eps, new_center, new_hw)) {
                        // there's an element in i-th subtree that's in the eps-vicinity
                        // => cannot insert the element
                        return ROOT_NODE;
                    }
                }
            }

            // no collisions, check quadrant
            if (isLeaf(parent.children[leaf_quadrant])) {
                if (!check_leaf(parent_node_idx, leaf_quadrant, x_internal, eps, new_center,
                    new_hw)) {
                    // there's an element in i-th subtree that's in the eps-vicinity
                    // => cannot insert the element
                    return ROOT_NODE;
                }

                // found leaf to insert into
                std::unique_lock lock(parent.mutexes[leaf_quadrant]);
                if (isLeaf(parent.children[leaf_quadrant])) {
                    // at this point the target leaf is found and the appropriate mutex is locked
                    // try inserting the element
                    Node &parent = nodes[unsetInternalFlag(parent_node_idx)];
                    Leaf &leaf = leaves[parent.children[leaf_quadrant]];

                    // check leaf for collisions, find an empty space
                    int empty = -1;
                    {
                        InternalPoint element_internal;
                        for (uint32_t i = 0; i < _LEAF_SIZE; i++) {
                            if (leaf.children[i] != 0) {
                                pointToInternalPoint(elements[leaf.children[i]], element_internal);
                                if (Distance::leq(element_internal, x_internal, eps)) {
                                    // an element is too close
                                    // => cannot insert the element
                                    return ROOT_NODE;
                                }
                            } else {
                                // found an empty spot for inserting element into
                                empty = i;
                            }
                        }
                    }

                    // check if empty space is available
                    x_idx = safely_insert_element(x);
                    if (x_idx == ROOT_NODE) {
                        // inserting new element failed
                        return ROOT_NODE;
                    }
                    if (debug) std::cout << "Inserting point...\n";
                    if (empty < 0) {
                        // no space available => branch
                        InternalPoint leaf_center = center, leaf_hw = hw;
                        scalar_mult(leaf_hw, 0.5);
                        sub(leaf_center, leaf_hw);
                        masked_add(leaf_center, hw, leaf_quadrant);

                        uint32_t new_leaf = branch_leaf_and_insert(x_idx, x_internal, parent.children[leaf_quadrant], leaf_center, leaf_hw);
                        if (new_leaf == ROOT_NODE) {
                            // branching failed
                            return ROOT_NODE;
                        }
                        parent.children[leaf_quadrant] = new_leaf;
                    } else {
                        // insert new element into empty spot
                        leaf.children[empty] = x_idx;
                    }
                    break;
                }
            }

            // quadrant is a subtree => descend
            parent_node_idx = parent.children[leaf_quadrant];
            sub(center, new_hw);
            masked_add(center, hw, leaf_quadrant);
            hw = new_hw;
            leaf_quadrant = quadrant(x_internal, center);
        }

        // check if element was inserted into storage
        if (x_idx == ROOT_NODE) {
            return ROOT_NODE;
        }

        // check for collisions
        {
            InternalPoint center, hw;
            determine_center_and_halfwidth(bbmin, bbmax, center, hw);
            if (check_node(ROOT_NODE, x_internal, eps, center, hw, x_idx)) {
                // no collisions, return the index of the inserted element
                if (debug) std::cout << "Insertion successful!\n";
                return x_idx;
            }
        }

        // collision happened => remove element and abort
        if (debug || printCollisions) std::cout << "Conflict!\n";
        collisionCount += 1;
        remove_element(x_idx, x_internal, parent_node_idx, leaf_quadrant, center, hw);
        return ROOT_NODE;
    }

    /**
     * @brief Insert an element into the tree.
     *
     * Fix for when the given element is an rvalue (e.g. passing a constructor as a parameter).
     *
     * @param x element to be inserted
     * @param eps vicinity of x that should not contain any other element
     * @return index of inserted element if insertion was possible
     * @return 0x80000000 if insertion failed
     */
    uint32_t insert(const Point &&x, const DT eps = 0.) {
        Point v = std::move(x);
        return insert(v, eps);
    }

    /**
     * @brief checks the return value of insert function and tells whether it flagged success
     * @return true if insertion succeeded (\p insertReturn is a valid index)
     * @return false if insertion failed
     */
    static constexpr bool didInsertSucceed(size_t insertReturn) {
        return insertReturn < ROOT_NODE;
    }

    /**
     * @brief Return the number of points inside the tree
     *
     * @return uint32_t number of points inside the tree
     */
    uint32_t count() const {
        return elementCount;
    }

    /**
     * @brief Return the amount of storage the tree is taking up as a number of points
     *
     * @return uint32_t number of points corresponding to storage taken up
     */
    uint32_t size() const {
        return nElements;
    }

    /**
     * @brief Find k nearest neighbors of 'x' in the tree
     *
     * @warning Deletes everything from 'ret'!
     *
     * @param x reference point
     * @param[out] ret_indices container for returned indices
     * @param[out] ret_dists container for returned distances
     * @param k number of neighbors to return
     * @return true if k neighbors were found
     * @return false if less than k neighbors were found
     */
    bool nearest(const Point &x, std::vector<size_t> &ret_indices, std::vector<DT> &ret_dists, const unsigned int k = 1) const {
        // check whether the tree was initialized
        if (nodes.size() == 0) {
            throw std::runtime_error("[Polyp] tree was not initialized!");
        }

        // determine bounding box parameters
        InternalPoint center, hw;
        determine_center_and_halfwidth(bbmin, bbmax, center, hw);

        // create internal representation of x
        InternalPoint x_internal;
        pointToInternalPoint(x, x_internal);

        // prepare the return container
        ret_indices.clear();
        // one extra element for insertion (see 'nearest_insert_into_vector')
        ret_indices.reserve(k + 1);
        ret_dists.clear();
        ret_dists.reserve(k + 1);

        // start search at the root
        nearest_find(ROOT_NODE, center, hw, k, x_internal, ret_indices, ret_dists);

        // check whether 'ret_indices' and 'ret_dists' contain the same number of elements
        if (ret_indices.size() != ret_dists.size()) {
            throw std::runtime_error("[Polyp] k-nn: indices and distances do not match!");
        }

        // return whether all k neighbors were found
        return ret_indices.size() == k;
    }

    /**
     * @brief Find k nearest neighbors of 'x' in the tree
     *
     * Fix for when 'x' is an rvalue (e.g. passing a Point constructor as a parameter).
     *
     * @param x reference point
     * @param[out] ret_indices container for returned indices
     * @param[out] ret_dists container for returned distances
     * @param k number of neighbors to return
     * @return true if k neighbors were found
     * @return false if less than k neighbors were found
     */
    bool nearest(Point &&x, std::vector<size_t> &ret_indices, std::vector<DT> &ret_dists,
        const unsigned int k = 1) const {
        Point v = std::move(x);
        return nearest(v, ret_indices, ret_dists, k);
    }

    /**
     * @brief Find k nearest neighbors of 'x' in the tree
     *
     * @warning Deletes everything from 'ret'!
     *
     * @tparam k number of neighbors to return
     * @param x reference point
     * @param[out] ret_indices container for returned indices
     * @param[out] ret_dists container for returned distances
     * @return true if k neighbors were found
     * @return false if less than k neighbors were found
     */
    template <unsigned int k>
    bool nearest(const Point &x, std::array<size_t, k> &ret_indices, std::array<DT, k> &ret_dists) const {
        // check whether the tree was initialized
        if (nodes.size() == 0) {
            throw std::runtime_error("[Polyp] tree was not initialized!");
        }

        // determine bounding box parameters
        InternalPoint center, hw;
        determine_center_and_halfwidth(bbmin, bbmax, center, hw);

        // create internal representation of x
        InternalPoint x_internal;
        pointToInternalPoint(x, x_internal);

        if constexpr (k == 1) {
            // prepare the return container
            ret_indices[0] = 0;
            ret_dists[0] = std::numeric_limits<DT>::infinity();

            // start search at the root
            nearest_find(ROOT_NODE, center, hw, x_internal, ret_indices, ret_dists);

            // check whether the returned index has valid distance
            if (
                ret_indices[0] == 0 && ret_dists[0] < std::numeric_limits<DT>::infinity() ||
                ret_indices[0] > 0 && ret_dists[0] == std::numeric_limits<DT>::infinity()
            ) {
                throw std::runtime_error("[Polyp] k-nn: index and distance do not match!");
            }

            // return whether a neighbor was found
            return ret_indices[0] > 0;
        } else {
            // prepare the return container
            for (int i = 0; i < k; i++) {
                ret_indices[i] = 0;
                ret_dists[i] = std::numeric_limits<DT>::infinity();
            }

            // start search at the root
            nearest_find<k>(ROOT_NODE, center, hw, x_internal, ret_indices, ret_dists);

            // check whether all returned non-zero indices have finite distances
            for (int i = 0; i < k; i++) {
                if (ret_indices[i] == 0 && ret_dists[i] < std::numeric_limits<DT>::infinity()
                    || ret_indices[i] > 0 && ret_dists[i] == std::numeric_limits<DT>::infinity()) {
                    throw std::runtime_error("[Polyp] k-nn: indices and distances do not match!");
                }
            }

            // return whether all k neighbors were found
            return ret_indices[k - 1] > 0;
        }
    }

    /**
     * @brief Find k nearest neighbors of 'x' in the tree
     *
     * Fix for when 'x' is an rvalue (e.g. passing a Point constructor as a parameter).
     *
     * @tparam k number of neighbors to return
     * @param x reference point
     * @param[out] ret_indices container for returned indices
     * @param[out] ret_dists container for returned distances
     * @return true if k neighbors were found
     * @return false if less than k neighbors were found
     */
    template <unsigned int k>
    bool nearest(Point &&x, std::array<size_t, k> &ret_indices, std::array<DT, k> &ret_dists) const {
        Point v = std::move(x);
        return nearest<k>(v, ret_indices, ret_dists);
    }

    /**
     * @brief Get the reference to a Point in the tree, defined with the index 'idx'.
     *
     * @warning Only defined for indices redurned by 'nearest'!
     *
     * @param idx valid index of a point in the tree
     * @return const Point& reference to the point
     */
    const Point& getPoint(const size_t idx) const {
        // check whether the tree was initialized
        if (nodes.size() == 0) {
            throw std::runtime_error("[Polyp] tree was not initialized!");
        }

        return elements[(uint32_t) idx];
    }

    /**
     * @brief Construct an input iterator pointing to the first element in the tree.
     *
     * @return PolypIterator the iterator pointing to the first element in the tree
     */
    PolypIterator begin() const {
        PolypIterator ret(*this);
        uint32_t internal_node = ROOT_NODE, internal_idx = 0;

        while (true) {
            ret.tree_path.push({internal_node, internal_idx});
            const Node& node = nodes[unsetInternalFlag(internal_node)];
            if (isLeaf(node.children[internal_idx])) {
                ret.leaf_idx = node.children[internal_idx];
                ret.leaf_loc = -1;
                ret.leaf_lock = std::shared_lock(node.mutexes[internal_idx]);
                break;
            } else {
                internal_node = node.children[internal_idx];
            }
        }

        // find first valid location
        return ++ret;
    }

    /**
     * @brief Construct an input iterator pointing just pust the last element in the tree.
     *
     * @return PolypIterator the iterator pointing just pust the last element in the tree
     */
    PolypIterator end() const {
        PolypIterator ret(*this);
        // first position "outside" the tree
        ret.tree_path.push({ROOT_NODE, CHILDREN});
        return ret;
    }

    // export iterator type
    typedef PolypIterator iterator;

}; // class Polyp



/**
 * @brief Wrapper class containing debugging functions.
 *
 * @tparam Tree tree type
 */
template <class Tree>
class PolypDebugger {

private:
    /**
     * @brief Construct a string representing the given node index.
     *
     * @param node index to represent
     * @param width minimum number of charactersin string
     * @return std::string the formatted string
     */
    static std::string format_node_string(int node, int width = 5) {
        std::string ret =
            Tree::isLeaf(node) ?
            (Tree::isEmpty(node) ? "E" : "L" + std::to_string(node)) :
            "I" + std::to_string(Tree::unsetInternalFlag(node));
        ret.append(width - ret.length(), ' ');
        return ret;
    }

public:
    /**
     * @brief Print the structure of the tree to the standard output.
     *
     * @param tree tree to print
     */
    static void print(Tree &tree) {
        std::cout << "Bounding box: [" << tree.bbmin[0] << "," << tree.bbmax[0] << "]";
        for (int i = 1; i < tree.DIM; i++) {
            std::cout << "×[" << tree.bbmin[i] << "," << tree.bbmax[i] << "]";
        }
        typename Tree::InternalPoint temp, widths;
        Tree::pointToInternalPoint(tree.bbmin, temp);
        Tree::pointToInternalPoint(tree.bbmax, widths);
        Tree::sub(widths, temp);
        std::cout << " (" << widths[0];
        for (int i = 1; i < Tree::DIM; i++) {
            std::cout << " by " << widths[i];
        }
        std::cout << ")" << std::endl;
        std::cout << "Elements: " << (tree.nElements - 1) << std::endl;
        if (tree.nElements > 1) {
            for (int i = 1; i < tree.nElements; i++) {
                std::cout << "  (";
                for (int j = 0; j < Tree::DIM; j++) {
                    if (j > 0) std::cout << ",";
                    std::cout << tree.elements[i][j];
                }
                std::cout << ")" << std::endl;
            }
        }
        std::cout << "Tree:" << std::endl;
        for (int i = 0; i < tree.nNodes; i++) {
            std::cout << "  ";
            for (uint32_t j = 0; j < Tree::CHILDREN; j++) {
                std::cout << format_node_string(tree.nodes[i].children[j]);
            }
            std::cout << std::endl;
        }

        std::cout << std::endl;
    }

    /**
     * @brief Serialize a subtree into a JSON string.
     *
     * @param tree tree to serialize
     * @param node node representing the subtree
     * @return std::string serialized JSON string
     */
    static std::string as_JSON(Tree &tree, int node = Tree::ROOT_NODE) {
        if (Tree::isLeaf(node)) {
            typename Tree::Leaf &leaf = tree.leaves[node];
            std::string ret = "[";
            for (int i = 0; i < Tree::_LEAF_SIZE; i++)
                if (leaf.children[i] > 0) {
                    ret += std::to_string(leaf.children[i]);
                    ret += ",";
                }
            ret += "]";
            return ret;
        } else {
            std::string ret = "[";
            for (int i = 0; i < Tree::CHILDREN; i++) {
                if (i > 0) ret += ",";
                ret += as_JSON(tree, tree.nodes[Tree::unsetInternalFlag(node)].children[i]);
            }
            ret += "]";
            return ret;
        }
    }

    /**
     * @brief Determine the maximum depth of a subtree.
     *
     * @param tree tree in which the subtree is contained
     * @param node node representing the subtree
     * @return int maximum depth of a node in the subtree
     */
    static int max_depth(Tree &tree, int node = 0) {
        int ret = 1;
        for (int i = 0; i < Tree::CHILDREN; i++) {
            int curr;
            if (!Tree::isLeaf(tree.nodes[node].children[i])) {
                curr = max_depth(tree, Tree::unsetInternalFlag(tree.nodes[node].children[i]));
                if (curr + 1 > ret) {
                    ret = curr + 1;
                }
            }
        }
        return ret;
    }

    /**
     * @brief Fill the tree fullness histogram.
     *
     * A tree fullness histogram is an array of counters of size Tree::_LEAF_SIZE + 1.
     * The counter at position i represents the number of leaves in the tree which contain exactly i elements.
     *
     * @param tree tree over which to construct a fullness histogram
     * @param histogram the array which stores the histogram
     * @param node node at which to begin construction
     */
    static void leaf_fullness_histogram(Tree &tree, std::array<uint64_t, Tree::_LEAF_SIZE + 1> &histogram, int node = Tree::ROOT_NODE) {
        if (Tree::isLeaf(node)) {
            const typename Tree::Leaf &leaf = tree.leaves[node];
            int total = 0;
            for (int i = 0; i < Tree::_LEAF_SIZE; i++) {
                if (!Tree::isEmpty(leaf.children[i])) {
                    total += 1;
                }
            }
            histogram[total] += 1;
        } else {
            const typename Tree::Node &curr_node = tree.nodes[Tree::unsetInternalFlag(node)];
            for (int i = 0; i < Tree::CHILDREN; i++) {
                leaf_fullness_histogram(tree, histogram, curr_node.children[i]);
            }
        }
    }

    /**
     * @brief Print distances of a point from the tree.
     *
     * @tparam Point type of the point
     * @param tree reference tree
     * @param pos position of the point
     * @param center center of the tree
     * @param hw halves of the widths of the tree
     * @param quadrant quadrant of the tree the point resides in
     */
    template <typename Point>
    static void distances(Tree &tree, Point pos, Point center, Point hw, int quadrant) {
        Point dists;
        Tree::distances(dists, pos, center, hw, quadrant);
        std::cout << "[";
        for (int i = 0; i < Tree::DIM; i++) {
            if (i > 0) std::cout << ",";
            std::cout << dists[i];
        }
        std::cout << "]\n";
    }

    /**
     * @brief Set the value of the tree's debug flag.
     *
     * @param tree the tree to modify
     * @param val desired value for the debug flag
     */
    static void setDebug(Tree &tree, bool val) {
        tree.debug = val;
    }

    /**
     * @brief Set the value of the tree's 'print collisions' flag.
     *
     * @param tree the tree to modify
     * @param val desired value for the 'print collisions' flag
     */
    static void setPrintCollisions(Tree &tree, bool val) {
        tree.printCollisions = val;
    }

    /**
     * @brief Get the number of collisions detected by the tree.
     *
     * @param tree the tree to inspect
     */
    static uint32_t getCollisionCount(Tree &tree) {
        return tree.collisionCount;
    }
};

} // namespace polyp
