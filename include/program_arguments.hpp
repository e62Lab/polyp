#pragma once

#include <map>
#include <functional>
#include <string>


template<class Key, class Val>
class TriggerMap {
    std::map<Key, std::function<void(Val)>> triggerMap;

public:
    void addCallback(const Key& key, std::function<void(Val)> callback) {
        triggerMap[key] = callback;
    }
    bool trigger(const Key& key, Val val) {
        auto it = triggerMap.find(key);
        if (it != triggerMap.end()) {
            it->second(val);
            return true;
        }
        return false;
    }
};


template<class Key>
class TriggerMap<Key, void> {
    std::map<Key, std::function<void()>> triggerMap;

public:
    void addCallback(const Key& key, std::function<void()> callback) {
        triggerMap[key] = callback;
    }

    bool trigger(const Key& key) {
        auto it = triggerMap.find(key);
        if (it != triggerMap.end()) {
            it->second();
            return true;
        }
        return false;
    }
};

class ProgramArguments : TriggerMap<std::string, void>, TriggerMap<std::string, std::string>, TriggerMap<int, std::string> {
    int argc;
    char** argv;
    std::function<void(int, std::string)> defaultCallback;

public:
    ProgramArguments(int argc, char** argv) : argc(argc), argv(argv) {}

    using TriggerMap<std::string, void>::addCallback;
    using TriggerMap<std::string, std::string>::addCallback;
    using TriggerMap<int, std::string>::addCallback;

    void addDefaultCallback(std::function<void(int, std::string)> func) {
        defaultCallback = func;
    }

    void parse() {
        for (int i = 1; i < argc; ++i) {
            if (TriggerMap<std::string, void>::trigger(argv[i])) {
                continue;
            }
            if (((i+1) < argc) && TriggerMap<std::string, std::string>::trigger(argv[i], argv[i+1])) {
                ++i;
                continue;
            }
            if (TriggerMap<int, std::string>::trigger(i, argv[i]) ) {
                continue;
            }
            defaultCallback(i, argv[i]);
        }
    }
};