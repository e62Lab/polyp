#include "../../include/polyp.hpp"
#include <cassert>
#include <iostream>

int main() {
    std::array<double, 2> bbox[] = {{-1., -1.}, {1., 1.}};
    polyp::Polyp<> tree(bbox[0], bbox[1]);
    typedef typename polyp::PolypDebugger<polyp::Polyp<>> dbg;

    // empty tree is a single internal node with 4 empty leaves
    assert((dbg::as_JSON(tree) == "[[],[],[],[]]"));

    std::cout << "OK\n";

    return 0;
}