#include "../../include/polyp.hpp"
#include <cassert>
#include <iostream>

int main() {
    std::array<double, 2> bbox[] = {{-1., -1.}, {1., 1.}};
    polyp::Polyp<> tree(bbox[0], bbox[1]);
    typedef typename polyp::PolypDebugger<polyp::Polyp<>> dbg;

    // insertion into empty tree should succeed
    assert((tree.insert({0., 0.}) != 0x80000000));
    // zero is exactly in the middle of the bounding box, should land in quadrant 0
    assert((dbg::as_JSON(tree) == "[[1,],[],[],[]]"));

    std::cout << "OK\n";

    return 0;
}