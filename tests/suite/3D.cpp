#include "../../include/polyp.hpp"
#include <cassert>

int main() {
    std::array<double, 3> bbox[] = {{0., 0., 0.}, {1., 1., 1.}};
    typedef polyp::Polyp<polyp::DefaultPolypTraits<3>> Tree;
            typedef polyp::PolypDebugger<Tree> DBG;
    Tree tree(bbox[0], bbox[1]);

    // array of 25 points
    std::array<double, 3> pts[] = {
        {0.33771249644547274, 0.55055471271353570, 0.59858947507418960},
        {0.04954930555566750, 0.89267138990539350, 0.92614424575812300},
        {0.45977415210148886, 0.06803873491350965, 0.54082633136014110},
        {0.96661212634774650, 0.23678021769075386, 0.89175733907320560},
        {0.94594604411326280, 0.41682639086214100, 0.67497871210145810},
        {0.04104174368471036, 0.34125166718221656, 0.15197932915742318},
        {0.47321014991088206, 0.21476987115079238, 0.12500401250468185},
        {0.56352618573916560, 0.88754880187310370, 0.25957836062811770},
        {0.25700487682375560, 0.21832619349653037, 0.28437261111526980},
        {0.70334747699655890, 0.80559451915499940, 0.23500279727187090},
        {0.28606260132296610, 0.90539607271238510, 0.47948263489919540},
        {0.40386517449571780, 0.80527298165560300, 0.27376139776950614},
        {0.19238679249306223, 0.87977390678188060, 0.09750853491649947},
        {0.38082498989598923, 0.46128040995420470, 0.38763279855306230},
        {0.84440078159629220, 0.54566053716901710, 0.55427050348211270},
        {0.62159779559396780, 0.65523174771159760, 0.24056881001219876},
        {0.13162570607396784, 0.57865420236543240, 0.41770928644761285},
        {0.27332979632685817, 0.80973196316029970, 0.70309354552118200},
        {0.47578511269609836, 0.18841120165513536, 0.01475048978392368},
        {0.94672044543535840, 0.38531589669518950, 0.86737203532195670},
        {0.63672139696042400, 0.50615842921061160, 0.92443170424466300},
        {0.16827131583319588, 0.89922379629910760, 0.29559342896201746},
        {0.41636333498884160, 0.28941122307980560, 0.67150445759577830},
        {0.54569789841163010, 0.89195530951709530, 0.69956120658287270},
        {0.09632063879740205, 0.65694706521659410, 0.15610099903925300}
    };

    // desired spacing
    double eps = 0.3;

    // insert points sequentially
    for (int i = 0; i < 25; i++) {
        bool inserted = tree.insert(pts[i], eps) != 0x80000000;
        if (i == 4 || i == 8 || i == 9 || i == 11 || i == 13 || i > 14 && i != 20 && i != 23) {
            // insertion should fail on these points
            assert((!inserted));
        } else {
            // insertion should succeed on these points
            assert((inserted));
        }
    }

    // look for 3 neighbors
    {
        std::vector<size_t> idxs;
        std::vector<double> dists;
        tree.nearest({0.5127, 0.8261, 0.8573}, idxs, dists, 3);

        // check whether the right neighbors are returned
        assert((idxs.size() == dists.size() && idxs.size() == 3));
        assert((idxs[0] == 12));
        assert((idxs[1] == 11));
        assert((idxs[2] == 1));
    }

    // look for 3 neighbors, with templated size
    {
        std::array<size_t, 3> idxs;
        std::array<double, 3> dists;
        tree.nearest<3>({0.5127, 0.8261, 0.8573}, idxs, dists);

        // check whether the right neighbors are returned
        assert((idxs[0] == 12));
        assert((idxs[1] == 11));
        assert((idxs[2] == 1));
        assert((dists[0] < 1.75));  // largest possible distance is sqrt(3)
        assert((dists[1] < 1.75));
        assert((dists[2] < 1.75));
    }

    // check specialization for 1 neighbor
    {
        std::array<size_t, 1> idxs;
        std::array<double, 1> dists;
        tree.nearest<1>({0.5127, 0.8261, 0.8573}, idxs, dists);

        // check whether the right neighbors are returned
        assert((idxs[0] == 12));
        assert((dists[0] < 1.75));
    }

    std::cout << "OK\n";

    return 0;
}