#include "../../include/polyp.hpp"
#include <cassert>
#include <iostream>

int main() {
    std::array<double, 2> bbox[] = {{-1., -1.}, {1., 1.}};
    polyp::Polyp<> tree(bbox[0], bbox[1]);
    typedef typename polyp::PolypDebugger<polyp::Polyp<>> dbg;

    std::array<double, 2> p1 = {0.3215, 0.22};
    std::array<double, 2> p2 = {0.3503, 0.1992};
    // Euclidean distance between p1 and p2: 0.03553...

    // insert a reference point, should succeed
    assert((tree.insert(p1) != 0x80000000));

    // inserting a point too close should fail
    assert((tree.insert(p2, 0.05) == 0x80000000));

    // only one point should be in the tree
    assert((dbg::as_JSON(tree) == "[[],[],[],[1,]]"));

    std::cout << "OK\n";

    return 0;
}