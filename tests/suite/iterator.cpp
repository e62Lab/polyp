#include "../../include/polyp.hpp"
#include <cassert>
#include <iostream>
#include <vector>

struct Traits : polyp::DefaultPolypTraits<2> {
    static constexpr int leaf_size = 2;
};

typedef polyp::Polyp<Traits> Tree;

int main() {
    std::array<double, 2> bbox[] = {{0., 0.}, {1., 1.}};

    Tree tree(bbox[0], bbox[1]);

    // array of 25 points
    std::array<double, 2> pts[] = {
        {0.561199792709660, 0.730248792267598},
        {0.881866500451810, 0.343877004114983},
        {0.669175304534394, 0.584069333278452},
        {0.190433267179954, 0.107769015243743},
        {0.368916546063895, 0.906308150649733},
        {0.460725937260412, 0.879653724481905},
        {0.981637950970750, 0.817760559370642},
        {0.156404952226563, 0.260727999055465},
        {0.855522805845911, 0.594356250664331},
        {0.644764536870088, 0.022512592740232},
        {0.376272210278832, 0.425259320214135},
        {0.190923695236303, 0.312718886820616},
        {0.428252992979386, 0.161484744311750},
        {0.482022061031856, 0.178766186752368},
        {0.120611613297162, 0.422885689100085},
        {0.589507484695059, 0.094229338887735},
        {0.226187679752676, 0.598523668756741},
        {0.384619124369411, 0.470924256358334},
        {0.582986382747674, 0.695949313301608},
        {0.251806122472313, 0.699887849928292},
        {0.290440664276979, 0.638530758271838},
        {0.617090884393223, 0.033603836066429},
        {0.265280909810029, 0.068806099118051},
        {0.824376266688835, 0.319599735180496},
        {0.982663399721950, 0.530864280694127}
    };

    // desired spacing
    double eps = 0.05;

    // insert points sequentially
    for (int i = 0; i < 25; i++) {
        bool inserted = (tree.insert(pts[i], eps) != 0x80000000);
        if (i == 17 || i == 18 || i == 21) {
            // insertion should fail on these points
            assert((!inserted));
        } else {
            // insertion should succeed on these points
            assert((inserted));
        }
    }

    // extract points into a vector
    std::vector<std::array<double, 2>> extracted = tree.extract_points();

    // traverse tree using an iterator
    std::vector<std::array<double, 2>> iterated;
    for (Tree::iterator it = tree.begin(); it != tree.end(); ++it) {
        iterated.push_back(*it);
    }

    // traverse using builtin insert method
    std::vector<std::array<double, 2>> inserted;
    inserted.insert(inserted.begin(), tree.begin(), tree.end());

    // check equality of returned vectors
    assert((extracted == iterated));
    assert((extracted == inserted));

    std::cout << "OK\n";

    return 0;
}