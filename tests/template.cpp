#include "../../include/polyp.hpp"
#include <cassert>

int main() {
    std::array<double, 2> bbox[] = {{-1., -1.}, {1., 1.}};
    polyp::Polyp<> tree(bbox[0], bbox[1]);

    // do stuff here
    // e.g. assert((dbg::as_JSON(tree) == "[0,0,0,0]"));

    std::cout << "OK\n";

    return 0;
}