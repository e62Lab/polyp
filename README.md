# Polyp

Parallel quadtree/octree/2^n-tree spatial indexing implementation.
Supports thread-safe point insertion with minimum distance and k-nearest neighbor lookup.

## Usage

The index is implemented in a single header file, `include/polyp.hpp`.
It depends on another header file, `include/vmc.hpp`, which should be kept along the Polyp source.
There are some usage examples in the `examples` folder.

Main functions of the index are point insertion with minimum distance and k-nearest neighbor lookup.
Both operations are thread-safe and minimally-blocking: Threads may only wait on the final leaf.

Many parameters of the index are defined as its traits.
Their definitions can be found in the `DefaultPolypTraits` struct, which is meant to act as a base for the final trait struct.
We recommend the final user to extend the `DefaultPolypTraits` struct with their own desired parameters, such as the point type.

## Compilation

For examples of how to use the index see folders `examples` and `tests/suite`.
A general example showing the most common use case for the index can be found in `generate_knn`.
It can be compiled using `make`:
```
make generate_knn
```
And exectuted using the following options:
```
-np N       try inserting N points (default: 1'000'000)

-nt K       spawn K threads for point insertion (default: 1)
```
For example:
```
./generate_knn -np 100000 -nt 4
```

The tests can be compiled and executed all at once with `make test`.

## Debugging wrapper

There is a `PolypDebugger` class available in order to aid in debugging the tree.
It contains functions that can alter internal debug values, and present the internal structure of the tree.
These functions are not intended to be used in production.