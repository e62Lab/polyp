#include "../include/polyp.hpp"
#include "../include/helper_funcs.hpp"
#include "../include/program_arguments.hpp"
#include <cassert>
#include <random>
#include <iostream>
#include <thread>

// typedef polyp::Polyp<polyp::DefaultPolypTraits<2>> tree_t;
// typedef polyp::Polyp<polyp::DefaultPolypTraits<2>> *tree_ptr;
typedef polyp::Polyp<> tree_t;
typedef polyp::Polyp<> *tree_ptr;
typedef typename polyp::PolypDebugger<tree_t> dbg;

void fillTree(tree_ptr tree, int num_points, const double eps) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1., 1.);
    for (int n = 0; n < num_points; n++) {
        // std::cout << n << std::endl;
        std::array<double, 2> element = {dis(gen), dis(gen)};
        // tree->insert(element, eps * exp(element[0]*element[0] + element[1]*element[1]));
        tree->insert(element, eps);
    }
}

int main(int argc, char **argv) {
    /// Parse command line arguments
    ProgramArguments arguments(argc, argv);
    arguments.addDefaultCallback([](int i, const std::string s){std::cout << "unknown command " << i << "-th line parameter '" << s << "'.\n";});
    int num_threads = 1;
    arguments.addCallback("-nt", [&num_threads](const std::string& val){num_threads = std::stoi(val);});
    int num_points = 1'000'000;
    arguments.addCallback("-np", [&num_points](const std::string& val){num_points = std::stoi(val);});

    std::cout << "============================================\n";
    arguments.parse();
    std::cout << "= number of threads:   " << num_threads << "\n";
    std::cout << "= number of points:    " << num_points  << "\n";
    int elements_per_thread = (num_points+num_threads-1) / num_threads; // round up
    double eps = 20 / std::sqrt(num_points);
    std::cout << "= ϵ:                   " << eps  << "\n";
    std::cout << "============================================\n"; 

    /// start packing the nodes into the tree
    std::array<double, 2> bbox[] = {{-1., -1.}, {1., 1.}};
    // deliberately not enough points in order to trigger the resize
    tree_ptr tree = new tree_t(bbox[0], bbox[1]);
    dbg::setPrintCollisions(*tree, false);
    std::cout << "Created tree.\n";

    // tries filling up a tree with some random elements
    std::vector<std::thread> threads;
    threads.resize(num_threads);

    for (int i = 0; i < num_threads; i++) {
        threads[i] = std::move(std::thread(fillTree, tree, elements_per_thread, eps));
    }

    for (int i = 0; i < num_threads; i++) {
        threads[i].join();
    }

    // some data about the tree
    std::cout << "maximum depth: " << dbg::max_depth(*tree) << std::endl;

    // find k nearest neighbors around a point
    int k = 10;
    std::array<double, 2> ref_point = {0.16643, -0.777};
    std::vector<size_t> indices;
    std::vector<double> distances;
    tree->nearest(ref_point, indices, distances, k);

    std::cout << "Neighbors:\n";
    for (int i = 0; i < indices.size(); i++) {
        std::cout << indices[i] << " -> " << distances[i] << std::endl;
    }

    return 0;
}
