#include "../include/polyp.hpp"
#include "../include/helper_funcs.hpp"
//#include "../../nanoflann/include/nanoflann.hpp"
#include "../../nanoflann/include/nanoflann.hpp"
#include <cassert>
#include <random>
#include <iostream>
#include <chrono>

struct PointCloud
{
    std::vector<std::array<double, 2>> pts;

    inline size_t kdtree_get_point_count() const { return pts.size(); }

    inline double kdtree_get_pt(const size_t idx, const size_t dim) const
    {
        return pts[idx][dim];
    }

    template <class BBOX>
    bool kdtree_get_bbox(BBOX& /* bb */) const { return false; }
};

typedef nanoflann::KDTreeSingleIndexDynamicAdaptor<nanoflann::L2_Simple_Adaptor<double, PointCloud>,
    PointCloud, 2> my_kd_tree_t;

typedef decltype(std::chrono::high_resolution_clock::now()) time_type;
typedef std::chrono::microseconds micros;
typedef decltype(std::chrono::duration_cast<micros>(
    std::chrono::high_resolution_clock::now() - std::chrono::high_resolution_clock::now()))
    duration_t;
const auto zero_duration = duration_t::zero;
const auto now = std::chrono::high_resolution_clock::now;
const long num_insertions = 200000;
const double eps = 0.02;
const long repeats = 50;
const long print_every = 5;

void test_nanoflann() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1., 1.);
    std::vector<std::array<double, 2>> extracted;
    extracted.reserve(num_insertions);

    duration_t setup_time = zero_duration();
    duration_t insertion_time = zero_duration();
    duration_t extraction_time = zero_duration();
    time_type start, end;

    long numPlacedNanoflann = 0;

    for (int i = 0; i < repeats; i++) { // nanoflann
        if (i % print_every == 0) std::cout << "." << std::flush;
        // setup
        start = now();
        PointCloud cloud;
        cloud.pts.reserve(num_insertions);
        my_kd_tree_t tree(2, cloud);
        nanoflann::KNNResultSet<double> resultSet(1);
        size_t ret_index;
        double out_dist_sqr;
        resultSet.init(&ret_index, &out_dist_sqr);
        end = now();
        setup_time += std::chrono::duration_cast<micros>(end - start);

        // insertion
        start = now();
        for (int n = 0; n < num_insertions; n++) {
            std::array<double, 2> element = {dis(gen), dis(gen)};
            resultSet.init(&ret_index, &out_dist_sqr);
            tree.findNeighbors(resultSet, &element[0], nanoflann::SearchParams(1));
            if (n == 0 || out_dist_sqr > eps * eps) {
                cloud.pts.push_back(element);
                size_t last = cloud.pts.size() - 1;
                tree.addPoints(last, last);
                ++numPlacedNanoflann;
            }
        }
        end = now();
        insertion_time += std::chrono::duration_cast<micros>(end - start);

        // extraction
        extracted.clear();
        start = now();
        extracted = std::move(cloud.pts);
        end = now();
        extraction_time += std::chrono::duration_cast<micros>(end - start);
    }

    double setup_ms = setup_time.count() / (double) repeats / num_insertions;
    double insertion_ms = insertion_time.count() / (double) repeats / num_insertions;
    double extraction_ms = extraction_time.count() / (double) repeats / num_insertions;

    // check correctness
    double min_dist = minimum_distance<2>(extracted);
    save_for_matlab<2>(extracted, "extracted_nanoflann.txt");

    std::cout << " Nanoflann:\n";
    std::cout << "Minimum distance: " << min_dist << std::endl;
    std::cout << "Setup: " << setup_ms << " ms, Insertion: " << insertion_ms << " ms, Extraction: "
        << extraction_ms << " ms, num inserted: " << numPlacedNanoflann << ".\n";
}

template <int size_of_leaf> struct PolypTraits : polyp::DefaultPolypTraits<2> {
    static constexpr int leaf_size = size_of_leaf;
};

template <int leaf_size> void test_quadtree() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1., 1.);
    std::vector<std::array<double, 2>> extracted;
    extracted.reserve(num_insertions);

    duration_t setup_time = zero_duration();
    duration_t insertion_time = zero_duration();
    duration_t extraction_time = zero_duration();
    time_type start, end;

    long numPlacedCas = 0;

    for (int i = 0; i < repeats; i++) { // quadtree
        if (i % print_every == 0) std::cout << "." << std::flush;
        // setup
        start = now();
        std::array<double, 2> bbox[] = {{-1., -1.}, {1., 1.}};
        polyp::Polyp<PolypTraits<leaf_size>> tree(bbox[0], bbox[1]);
        end = now();
        setup_time += std::chrono::duration_cast<micros>(end - start);

        // insertion
        start = now();
        for (int n = 0; n < num_insertions; n++) {
            std::array<double, 2> element = {dis(gen), dis(gen)};
            if (0x80000000 > tree.insert(element, eps))
                ++numPlacedCas;
        }
        end = now();
        insertion_time += std::chrono::duration_cast<micros>(end - start);

        // extraction
        extracted.clear();
        start = now();
        tree.extract_points(extracted);
        end = now();
        extraction_time += std::chrono::duration_cast<micros>(end - start);
    }

    double setup_ms = setup_time.count() / (double) repeats / num_insertions;
    double insertion_ms = insertion_time.count() / (double) repeats / num_insertions;
    double extraction_ms = extraction_time.count() / (double) repeats / num_insertions;

    // check correctness
    double min_dist = minimum_distance<2>(extracted);
    save_for_matlab<2>(extracted, "extracted_quadtree.txt");

    std::cout << " Polyp (leaf size " << leaf_size << "):\n";
    std::cout << "Minimum distance: " << min_dist << std::endl;
    std::cout << "Setup: " << setup_ms << " ms, Insertion: " << insertion_ms << " ms, Extraction: "
        << extraction_ms << " ms, num inserted: " << numPlacedCas << ".\n";
}

int main() {
    test_nanoflann();

    test_quadtree<1>();
    test_quadtree<2>();
    test_quadtree<4>();
    test_quadtree<8>();
    test_quadtree<16>();
    test_quadtree<32>();

    return 0;
}
