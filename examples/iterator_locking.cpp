#include "../include/polyp.hpp"
#include "../include/helper_funcs.hpp"
#include <cassert>
#include <random>
#include <iostream>
#include <thread>

// typedef polyp::Polyp<polyp::DefaultPolypTraits<2>> tree_t;
// typedef polyp::Polyp<polyp::DefaultPolypTraits<2>> *tree_ptr;
typedef polyp::Polyp<> tree_t;
typedef polyp::Polyp<> *tree_ptr;
typedef typename polyp::PolypDebugger<tree_t> dbg;

const int num_points = 1'000'000;
const int num_threads = 8;

void fillTree(tree_ptr tree, int num_points, double eps) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1., 1.);
    for (int n = 0; n < num_points; n++) {
        // std::cout << n << std::endl;
        std::array<double, 2> element = {dis(gen), dis(gen)};
        // tree->insert(element, eps * exp(element[0]*element[0] + element[1]*element[1]));
        tree->insert(element, eps);
    }
}

int main() {
    std::array<double, 2> bbox[] = {{-1., -1.}, {1., 1.}};
    tree_ptr tree = new tree_t(bbox[0], bbox[1], num_points);
    // tree->printCollisions = true;

    // fill at some eps
    std::vector<std::thread> threads;
    threads.resize(num_threads);
    int elements_per_thread = num_points / num_threads;

    for (int i = 0; i < num_threads; i++) {
        threads[i] = std::move(std::thread(fillTree, tree, elements_per_thread, 0.002));
    }

    for (int i = 0; i < num_threads; i++) {
        threads[i].join();
    }

    // iterate over while filling at way smaller eps (e.g. eps == 0.01)
    for (int i = 0; i < num_threads; i++) {
        threads[i] = std::move(std::thread(fillTree, tree, elements_per_thread, 0.0005));
    }
    std::vector<std::array<double, 2>> points;
    for (tree_t::iterator it = tree->begin(); it != tree->end(); ++it) {
        points.push_back(*it);
    }


    for (int i = 0; i < num_threads; i++) {
        threads[i].join();
    }

    // saves to a file
    save_for_matlab<2>(points, "threaded_iterator_test.txt");

    return 0;
}