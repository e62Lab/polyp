#include "../include/polyp.hpp"
#include "../include/helper_funcs.hpp"
#include <cassert>
#include <random>
#include <iostream>

int main() {
    std::array<double, 2> bbox[] = {{-1., -1.}, {1., 1.}};
    polyp::Polyp<> *tree = new polyp::Polyp<>(bbox[0], bbox[1]);
    typedef typename polyp::PolypDebugger<polyp::Polyp<>> dbg;

    // tries filling up a tree with a million random elements
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1., 1.);
    double eps = 0.015;
    for (int n = 0; n < 1000000; n++) {
        // std::cout << n << std::endl;
        std::array<double, 2> element = {dis(gen), dis(gen)};
        // tree->insert(element, eps * exp(element[0]*element[0] + element[1]*element[1]));
        tree->insert(element, eps);
    }

    // tree.debug = true;
    // tree.insert({ 0.0001,  0.0001}, eps);
    // tree.insert({ 0.0001, -0.0001}, eps);
    // tree.insert({-0.0001,  0.0001}, eps);
    // tree.insert({-0.0001, -0.0001}, eps);

    // saves to a file
    std::cout << dbg::max_depth(*tree) << std::endl;
    std::vector<std::array<double, 2>> extracted = tree->extract_points();
    std::cout << minimum_distance<2>(extracted) << std::endl;
    save_for_matlab<2>(extracted, "1M_test.txt");

    return 0;
}