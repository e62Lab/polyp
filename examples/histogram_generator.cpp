#include "../include/polyp.hpp"
#include "../include/helper_funcs.hpp"
#include "../include/program_arguments.hpp"
#include <cassert>
#include <random>
#include <iostream>
#include <fstream>
#include <thread>
#include <vector>
#include <filesystem>
#include <ctime>


template <int size_of_leaf> struct PolypTraits : polyp::DefaultPolypTraits<2> {
    static constexpr int leaf_size = size_of_leaf;
};
typedef decltype(std::chrono::high_resolution_clock::now()) time_type;
typedef std::chrono::microseconds micros;
typedef decltype(std::chrono::duration_cast<micros>(
    std::chrono::high_resolution_clock::now() - std::chrono::high_resolution_clock::now()))
    duration_t;
const auto now = std::chrono::high_resolution_clock::now;

int num_points = 100'000;
double eps = 20 / std::sqrt(num_points);
int num_threads = 1;
int num_iterations = 1;
std::string hostname = "";
double (*eps_f)(double, double, double);
std::string eps_f_name = "";


// helper functions

typedef std::pair<std::string, std::string> param_t;
typedef std::array<double, 3> result_t;

size_t save_results(std::vector<param_t> &params, std::vector<result_t> &results) {
    namespace fs = std::filesystem;
    fs::create_directory("results");

    size_t timestamp = std::time(nullptr);
    std::ofstream output_file;
    output_file.open("results/" + std::to_string(timestamp) + ".json");

    output_file << "{\"params\": {\n";
    for (param_t s : params) {
        output_file << "    \"" << s.first << "\": \"" << s.second << "\",\n";
    }
    size_t pos = output_file.tellp();
    output_file.seekp(pos-2);
    output_file << std::endl;

    output_file << "}, \"results\": [\n";
    for (result_t r : results) {
        output_file << "    ["
            << std::to_string(r[0]) << ","
            << std::to_string(r[1]) << ","
            << std::to_string(r[2])
            << "],\n";
    }
    pos = output_file.tellp();
    output_file.seekp(pos-2);
    output_file << std::endl;
    output_file << "]}\n";

    output_file.close();
    return timestamp;
}


// fill density functions

inline double eps_flat(double base, double x, double y) {
    return base;
}

inline double eps_linear(double base, double x, double y) {
    return pow(2., x) * base;
}

inline double eps_hotspots(double base, double x, double y) {
    // sqrt((x-0.5)² + (y-0.1)²)
    // sqrt((x+0.5)² + (y+0.1)²)
    return (pow(30.,
        sqrt((x-0.8)*(x-0.8) + (y-0.8)*(y-0.8))
        * sqrt((x+0.8)*(x+0.8) + (y-0.3)*(y-0.3))
        * sqrt((x-0.2)*(x-0.2) + (y+0.8)*(y+0.8)))) * 0.15 * base;
}


// experiment functions

template <typename tree_t>
void fillTree(tree_t *tree, int num_points) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1., 1.);
    for (int n = 0; n < num_points; n++) {
        std::array<double, 2> element = {dis(gen), dis(gen)};
        double curr_eps = eps_f(eps, element[0], element[1]);
        tree->insert(element, curr_eps);
    }
}

template <typename tree_t, typename histogram_t>
result_t experiment_quadtree(histogram_t &histogram) {
    std::vector<std::array<double, 2>> extracted;
    extracted.reserve(num_points);

    time_type start, end;

    // setup
    start = now();
    std::array<double, 2> bbox[] = {{-1., -1.}, {1., 1.}};
    tree_t tree(bbox[0], bbox[1]);
    end = now();
    duration_t setup_time = std::chrono::duration_cast<micros>(end - start);

    // insertion
    if (num_threads > 1) {
        std::vector<std::thread> threads;
        threads.resize(num_threads);
        int elements_per_thread = num_points / num_threads;

        start = now();
        for (int i = 0; i < num_threads; i++) {
            threads[i] = std::move(std::thread(fillTree<tree_t>, &tree, elements_per_thread));
        }
        for (int i = 0; i < num_threads; i++) {
            threads[i].join();
        }
        end = now();
    } else {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(-1., 1.);

        start = now();
        for (int n = 0; n < num_points; n++) {
            std::array<double, 2> element = {dis(gen), dis(gen)};
            double curr_eps = eps_f(eps, element[0], element[1]);
            tree.insert(element, curr_eps);
        }
        end = now();
    }
    duration_t insertion_time = std::chrono::duration_cast<micros>(end - start);

    // extraction
    extracted.clear();
    start = now();
    tree.extract_points(extracted);
    end = now();
    duration_t extraction_time = std::chrono::duration_cast<micros>(end - start);

    // update histogram
    typedef typename polyp::PolypDebugger<tree_t> dbg;
    dbg::leaf_fullness_histogram(tree, histogram);

    return {setup_time.count() / 1000., insertion_time.count() / 1000., extraction_time.count() / 1000.};
}



int main(int argc, char **argv) {
    // parse command line arguments
    ProgramArguments arguments(argc, argv);
    arguments.addCallback("--nThreads", [](const std::string& val){num_threads = std::stoi(val);});
    arguments.addCallback("--nPoints", [](const std::string& val){
        num_points = std::stoi(val);
        eps = 20 / std::sqrt(num_points);
    });
    arguments.addCallback("--eps", [](const std::string& val){eps = std::stod(val);});
    arguments.addCallback("--epsRelative", [](const std::string& val){eps = std::stod(val) / std::sqrt(num_points);});
    arguments.addCallback("--nIterations", [](const std::string& val){num_iterations = std::stoi(val);});
    arguments.addCallback("--host", [](const std::string& val){hostname = val;});
    eps_f = eps_flat;
    eps_f_name = "flat";
    bool needHelp = false;
    arguments.addCallback("--epsFunc", [&needHelp](const std::string& val){
        if (val == "flat") {
            eps_f = eps_flat;
            eps_f_name = "flat";
        } else if (val == "linear") {
            eps_f = eps_linear;
            eps_f_name = "linear";
        } else if (val == "hotspots") {
            eps_f = eps_hotspots;
            eps_f_name = "hotspots";
        } else {
            needHelp = true;
        }
    });
    arguments.addCallback("--help", [&needHelp](){needHelp = true;});
    arguments.addDefaultCallback([&needHelp](int i, const std::string s){needHelp = true;});

    arguments.parse();

    if (needHelp) {
        std::cout << "Options:\n";
        std::cout << "    --nThreads <n> => set number of threads simultaneously inserting points, default: 1, only applies to quadtree\n";
        std::cout << "    --nPoints <n> => set number of points to generate randomly and try to insert, default: 100'000\n";
        std::cout << "    --eps <d> => set minimum distance between any 2 points, absolute\n";
        std::cout << "    --epsRelative <d> => set minimum distance between any 2 points, relative to number of points, default 20 (gets divided by sqrt(num_points))\n";
        std::cout << "    --epsFunc <s> => set density function, options: flat (default), linear, hotspots\n";
        std::cout << "    --nIterations <n> => set number of experiments to execute, default: 1\n";
        std::cout << "    --host <name> => set optional host name, for later reference\n";
        std::cout << "    --help => print this and exit\n";

        return 0;
    }

    #ifndef LEAF_SIZE
    #define LEAF_SIZE 10
    #endif

    #if !(LEAF_SIZE - 0)
    #undef LEAF_SIZE
    #define LEAF_SIZE 10
    #endif

    typedef polyp::Polyp<PolypTraits<LEAF_SIZE>> tree_t;
    typedef std::array<uint64_t, LEAF_SIZE + 1> histogram_t;

    histogram_t histogram;
    for (int i = 0; i <= LEAF_SIZE; i++)
        histogram[i] = 0;

    for (int i = 0; i < num_iterations; i++) {
        experiment_quadtree<tree_t, histogram_t>(histogram);
    }

    std::cout << "[";
    for (int i = 0; i < LEAF_SIZE; i++)
        std::cout << histogram[i] << ",";
    std::cout << histogram[LEAF_SIZE] << "]\n";

    return 0;
}