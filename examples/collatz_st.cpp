#include <chrono>
#include <functional>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include "../include/vmc.hpp"

typedef decltype(std::chrono::high_resolution_clock::now()) time_type;
typedef std::chrono::microseconds micros;
typedef decltype(std::chrono::duration_cast<micros>(
    std::chrono::high_resolution_clock::now() -
    std::chrono::high_resolution_clock::now())) duration_t;
const auto zero_duration = duration_t::zero;
const auto now = std::chrono::high_resolution_clock::now;

template <class T>
using vector = std::vector<T>;

template <class V>
void checkLastEntries(V& v, long n, long cnt) {
  std::cout << "Last calculated values: ";
  for (size_t j = n - 1; j > n - cnt; --j)
    std::cout << v[j] << " ";
  std::cout << "\n";
}

inline long collatz(long n) {
  if (n & 1)
    return n * 3 + 1;
  return n >> 1;
}

///////////////////////////////////////////////////////////////////////////////////
// facade : make different classes look the same from the outside
template <template <class> class Container, class T>
struct Factory {
  static Container<T> construct(size_t initialSize, size_t maxSize) {
    return Container<T>(initialSize);
  }
};

template <class T>
struct Factory<vmc::VirtualMemoryContainer, T> {
  static vmc::VirtualMemoryContainer<T> construct(size_t initialSize,
                                             size_t maxSize) {
    return vmc::VirtualMemoryContainer<T>(maxSize);
  }
};

template <class Container>
void resize(Container& c, size_t size) {
  c.resize(size);
}

template <class T>
void resize(vmc::VirtualMemoryContainer<T>& c, size_t) {
  // no resizing necessary
}

template <class Container>
void reserve(Container& c, size_t size) {
  c.reserve(size);
}

template <class T>
void reserve(vmc::VirtualMemoryContainer<T>& c, size_t) {
  // reserve not possible
}
///////////////////////////////////////////////////////////////////////////////////

template <template <class> class Container>
void experiment(std::string name,
                long startSize,
                long max_size,
                long n,
                int repeats) {
  time_type start, end;
  double setup_ms, calc_ms, resize_ms;
  duration_t setup_time = zero_duration();
  duration_t calc_time = zero_duration();
  duration_t resize_time = zero_duration();
  size_t numResizes = 0;
  size_t finalSize = 0;
  size_t countRecurseCalls = 0;

  std::function<void(long, Container<long>&)> recurse;
  recurse = [&](long n, Container<long>& container) {
    countRecurseCalls++;
    long next = collatz(n);
    long delta = 1;

    while (next >= max_size) {
      next = collatz(next);
      delta += 1;
    }

    long larger = n > next ? n : next;
    if (larger >= container.size()) {
      auto t1 = now();
      resize(container, larger + 1);
      resize_time += std::chrono::duration_cast<micros>(now() - t1);
      ++numResizes;
    }

    if (next == 1) {
      container[n] = 1;  // one step to 1
    } else if (container[next] == 0) {
      recurse(next, container);
      container[n] = container[next] + delta;
    } else {
      container[n] = container[next] + delta;
    }
  };

  for (int i = 0; i < repeats; i++) {
    countRecurseCalls = 0;
    start = now();
    // allocate at least two elements, 0 and 1
    Container<long> container =
        Factory<Container, long>::construct(std::max(2L, startSize), max_size);
    // recursive calls should terminate in this element
    container[1] = 1;
    end = now();
    setup_time += std::chrono::duration_cast<micros>(end - start);

    start = now();
    for (int i = 1; i < n; i++) {
      recurse(i, container);
    }
    end = now();
    calc_time += std::chrono::duration_cast<micros>(end - start);

    finalSize = container.size();
#ifndef COMPACT_PRINT
    if (i == 0)
      checkLastEntries(container, n, 10);
#endif
  }

  setup_ms = setup_time.count() / (1000. * repeats);
  resize_ms = resize_time.count() / (1000. * repeats);
  calc_ms = calc_time.count() / (1000. * repeats) -
            resize_ms;  // resizing is included in the calculation time

#ifndef COMPACT_PRINT
  std::cout << "Experiment \x1B[33m" << std::setw(20) << name << ": "
            << std::setw(7) << setup_ms << " ms setup, " << std::setw(7)
            << calc_ms << " ms calculation in " << std::setw(7)
            << countRecurseCalls << " recursive calls, " << std::setw(7)
            << resize_ms << " ms for " << std::setw(5) << numResizes
            << " resizes, end size = " << finalSize << ".\033[0m\n";
#else
  std::cout << std::setw(19) << std::left << name << " "
            << std::setw(7) << std::right << setup_ms << " "
            << std::setw(7) << calc_ms << " "
            << std::setw(7) << countRecurseCalls << " "
            << std::setw(7) << resize_ms << " "
            << std::setw(7) << numResizes << " "
            << std::setw(9) << finalSize << std::endl;
#endif
}

/**
 * @brief Virtual memory container vs vector benchmark
 *
 * Generate a table of distances of the first n natural numbers from 1 for the
 * Collatz problem. Instead of calculating each number by itself, cache
 * previous results. Since some numbers jump outside of the initial range, the
 * container may need to be resized. We limit the maximum size of the
 * container to 1 GiB.
 */
int main(int argc, char** argv) {
  long n = 10000;
  int repeats = 10;
  // long max_size = 1024 * 1024 * 1024; // long is 8B => 8GiB
  long max_size = 128 * 1024 * 1024;  // long is 8B => 1GiB
  duration_t setup_time, calc_time;
  time_type start, end;
  double setup_ms, calc_ms;

  if (argc > 1) {
    n = atol(argv[1]);
  }
  if (argc > 2) {
    repeats = atol(argv[2]);
  }

  std::cout << "n = " << n << ", repeats = " << repeats << std::endl;

  experiment<vmc::VirtualMemoryContainer>("VMem", 1, max_size, n, repeats);
  experiment<vector>("vector", 1, max_size, n, repeats);
  experiment<vector>("vector(target_size)", n, max_size, n, repeats);
  experiment<vector>("vector(max_size)", max_size, max_size, n, repeats);

  return 0;
}