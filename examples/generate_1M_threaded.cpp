#include "../include/polyp.hpp"
#include "../include/helper_funcs.hpp"
#include <cassert>
#include <random>
#include <iostream>
#include <thread>

// typedef polyp::Polyp<polyp::DefaultPolypTraits<2>> tree_t;
// typedef polyp::Polyp<polyp::DefaultPolypTraits<2>> *tree_ptr;
typedef polyp::Polyp<> tree_t;
typedef polyp::Polyp<> *tree_ptr;
typedef typename polyp::PolypDebugger<tree_t> dbg;

const double eps = 0.015;
const int num_points = 1'000'000;
const int num_threads = 100;

void fillTree(tree_ptr tree, int num_points) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1., 1.);
    for (int n = 0; n < num_points; n++) {
        // std::cout << n << std::endl;
        std::array<double, 2> element = {dis(gen), dis(gen)};
        // tree->insert(element, eps * exp(element[0]*element[0] + element[1]*element[1]));
        tree->insert(element, eps);
    }
}

int main() {
    std::array<double, 2> bbox[] = {{-1., -1.}, {1., 1.}};
    tree_ptr tree = new tree_t(bbox[0], bbox[1], num_points);
    // tree->printCollisions = true;

    // tries filling up a tree with a million random elements
    std::vector<std::thread> threads;
    threads.resize(num_threads);
    int elements_per_thread = num_points / num_threads;

    for (int i = 0; i < num_threads; i++) {
        threads[i] = std::move(std::thread(fillTree, tree, elements_per_thread));
    }

    for (int i = 0; i < num_threads; i++) {
        threads[i].join();
    }

    // some data about the tree
    std::cout << "maximum depth: " << dbg::max_depth(*tree) << std::endl;
    // tree->_print();
    std::vector<std::array<double, 2>> points = std::move(tree->extract_points());
    std::cout << "total inserted: " << points.size() << std::endl;
    int offending = 0;
    std::cout << "minimum distance: " << minimum_distance<2>(points, eps, &offending) << std::endl;
    std::cout << "offenders: " << offending << std::endl;

    // saves to a file
    save_for_matlab<2>(points, "1M_test.txt");

    return 0;
}